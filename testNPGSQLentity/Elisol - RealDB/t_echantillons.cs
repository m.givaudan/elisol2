﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Elisol2.Entity_Data_Models.Elisol___MySQL
{
    [Table("t_echantillons", Schema = "public")]
    public class t_echantillons
    {
        [Key]
        public int id { get; set; }
        [ForeignKey("t_dossiers")]
        public Nullable<int> idossiers { get; set; }
        public string cnom { get; set; }
        public Nullable<int> iListeReduite { get; set; }     
       
        public virtual t_dossiers t_dossiers { get; set; }

        public virtual ICollection<t_liste_reduite> t_liste_reduite { get; set; }
        public virtual ICollection<t_dictee> t_dictee { get; set; }
    }
}
