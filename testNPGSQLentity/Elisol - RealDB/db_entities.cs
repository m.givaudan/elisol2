﻿using System.Data.Entity;

namespace Elisol2.Entity_Data_Models.Elisol___MySQL
{
    class db_entities : DbContext
    {
        public db_entities() : base(nameOrConnectionString: "BDD") { }

        public DbSet<t_clients> t_clients { get; set; }
        public DbSet<t_dictee> t_dictee { get; set; }
        public DbSet<t_dossiers> t_dossiers { get; set; }
        public DbSet<t_echantillons> t_echantillons { get; set; }
        public DbSet<t_liste_reduite> t_liste_reduite { get; set; }
        public DbSet<t_liste_reduite_nematode> t_liste_reduite_nematode { get; set; }
        public DbSet<t_nematodes> t_nematodes { get; set; }
        public DbSet<fos_user> fos_user { get; set; }
    }
}
