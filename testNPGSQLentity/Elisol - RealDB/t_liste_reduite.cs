﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Elisol2.Entity_Data_Models.Elisol___MySQL
{
    [Table("t_liste_reduite", Schema = "public")]
    public class t_liste_reduite
    {
        [Key]
        public int id { get; set; }
        public string cnom { get; set; }
        //[ForeignKey("tEchantillons")]
        //public Nullable<int> iID_Echantillon { get; set; }
        
        //public virtual t_Echantillons tEchantillons { get; set; }

       // public virtual ICollection<tNematodes> tNematodes { get; set; }
        public virtual ICollection<t_liste_reduite_nematode> t_liste_reduite_nematode { get; set; }

    }
}
