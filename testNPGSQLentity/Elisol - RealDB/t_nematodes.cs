﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Elisol2.Entity_Data_Models.Elisol___MySQL
{
    [Table("t_nematodes", Schema = "public")]
    public class t_nematodes
    {
        [Key]
        public int id { get; set; }
        public string cnom_complet { get; set; }
        public string cnom_dictee { get; set; }
        public string cnom_court { get; set; }
        public string cphonetique { get; set; }

        //public virtual ICollection<t_liste_reduite_nematode> t_liste_reduite_nematode { get; set; }
        //public virtual ICollection<t_dictee> t_dictee { get; set; }
    }
}
