﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Elisol2.Entity_Data_Models.Elisol___MySQL
{
    [Table("t_liste_reduite_rema", Schema = "public")]
    public class t_liste_reduite_nematode
    {
        [Key]
        public int id { get; set; }

        [Column(Order = 1)]
        [ForeignKey("t_nematodes")]
        public int iliste { get; set; }
        
        [Column(Order = 2)]
        [ForeignKey("t_liste_reduite")]
        public int inema { get; set; }

        public virtual t_nematodes t_nematodes { get; set; }
        public virtual t_liste_reduite t_liste_reduite { get; set; }
    }
}
