﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using Elisol2.Entity_Data_Models.Elisol___PostgreSQL_NPGSQL;

namespace testNPGSQLentity
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var c = new db_entities())
            {

                var listNem = c.tNematodes.ToList();
                foreach (var nema in listNem)
                {
                    Console.WriteLine(nema.cNom_Court);
                }

            }
            Console.ReadKey();
        }
    }
}
