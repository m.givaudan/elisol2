﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Elisol2.Entity_Data_Models.Elisol___PostgreSQL_NPGSQL
{
    [Table("tUtilisateurs", Schema = "public")]
    public class tUtilisateurs
    {
        [Key]
        public int iID_Utilisateurs { get; set; }
        public string cNom { get; set; }
        public string cPrenom { get; set; }
        public string cMail { get; set; }
        public string cRole { get; set; }
        public string pMot_de_Passe { get; set; }
        [ForeignKey("tDictee")]
        public Nullable<int> iID_Dictee { get; set; }

        
        public virtual tDictee tDictee { get; set; }
    }
}
