﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Elisol2.Entity_Data_Models.Elisol___PostgreSQL_NPGSQL
{
    [Table("tDossiers", Schema = "public")]
    public class tDossiers
    {
        [Key]
        public int iID_Dossiers { get; set; }
        [ForeignKey("tClients")]
        public Nullable<int> iClient { get; set; }
        public string cNom { get; set; }


        public virtual tClients tClients { get; set; }

        public virtual ICollection<tEchantillons> tEchantillons { get; set; }
    }
}
