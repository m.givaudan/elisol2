﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Elisol2.Entity_Data_Models.Elisol___MySQL
{
    [Table("tUtilisateurs", Schema = "public")]
    public class fos_user
    {
        [Key]
        public int id { get; set; }
        public string username { get; set; }
        public string username_canonical { get; set; }
        public string email { get; set; }
        public string email_canonical { get; set; }
        public bool enabled { get; set; }
        public string salt { get; set; }
        public string password { get; set; }
        public Nullable<System.DateTime> last_login { get; set; }
        public string confirmation_token { get; set; }
        public Nullable<System.DateTime> password_requested_at { get; set; }
        public string roles { get; set; }
        public string nom { get; set; }
        public string prenom { get; set; }
    }
}
