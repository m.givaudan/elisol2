﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Media;
using System.IO;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework;
using System.Globalization;
using System.Speech.AudioFormat;
using System.Speech.Recognition;

namespace Elisol

{
    class Reconnaissance
    {
        public static bool completed;
        public static string nematode;
        public static void RecognizeCompletedHandler(object sender, RecognizeCompletedEventArgs e)
        {
            string msg = "";
            if (e.Error != null)
            {
                msg = "  Erreur trouvée : " + e.Error.GetType().Name + ": " + e.Error.Message ;
            }
            if (e.Cancelled)
            {
                msg = "  Operation Annulée";
            }
            if (e.InputStreamEnded)
            {
                msg = "  Fin de la Reconnaissace";
            }

            completed= true;
        }
        public static void SpeechRecognizedHandler(object sender, SpeechRecognizedEventArgs e)
        {
            string msg = "";
            if (e.Result != null && e.Result.Text != null)
            {
                msg = "  Texte Reconnu = " +  e.Result.Text;
                nematode = e.Result.Text;
            }
            else
            {
                msg = "  Recognized text not available.";
            }
        }
    }
}
