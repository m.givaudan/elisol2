﻿using System;
using System.Collections.Generic;
using Elisol.Entity_Data_Models.ElisolModels;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Elisol
{
    public partial class Ajouter_liste_nematode : Form
    {
        List<t_nematodes> ListNema = new List<t_nematodes>();
        List<t_nematodes> n = new List<t_nematodes>();
        List<string> ListDGVListNema = new List<string>();
        List<string> ListDGVListNemaAdd = new List<string>();

        string up = "";
        string ClickedNema = "";
        string Pas2fois = "";
        string ClickedNemaAdd = "";
        string Pas2foisAdd = "";

        #region Au lancement de la WinFrom quand connecté à la BDD.
        public Ajouter_liste_nematode(List<t_liste_reduite_nematode> lrn, string nom)
        {
            InitializeComponent();

            up = nom;
            tbNomListe.Text = nom;

            using (db_entities context = new db_entities())
            {
                ListNema = context.t_nematodes.ToList();
                for (int i = 0; i < ListNema.Count; i++)
                {
                    ListDGVListNema.Add(ListNema[i].cnom_complet);
                }
               
                for (int i = 0; i < lrn.Count; i++)// remplis List<tNematodes> à partir de List<tListeReduiteNema>
                {
                    int id = Convert.ToInt32(lrn[i].inematodes);
                    n.Add(context.t_nematodes.FirstOrDefault(e => e.id == id));
                }
                
                foreach (t_nematodes i in n) // remplis ListDGVListNemaAdd avec List<tNematodes> et vire List<tNematodes> de ListDGVListNema
                {
                    ListDGVListNemaAdd.Add(i.cnom_complet);
                    ListDGVListNema.Remove(i.cnom_complet);
                }
                foreach (string i in ListDGVListNema)
                {
                    DGVListNema.Rows.Add(i);
                }
                foreach (string i in ListDGVListNemaAdd)
                {
                    DGVListNemaAdd.Rows.Add(i);
                }
            }
        }

        public Ajouter_liste_nematode()
        {
            InitializeComponent();

            #region Remplissage du datagridview.
            DGVListNema.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
            DGVListNemaAdd.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;

            using (db_entities context = new db_entities())
            {
                ListNema = context.t_nematodes.ToList();
                for (int i = 0; i < ListNema.Count; i++)
                {
                    DGVListNema.Rows.Add(ListNema[i].cnom_complet);
                    ListDGVListNema.Add(ListNema[i].cnom_complet);
                }
            }
            #endregion 
        }
        #endregion

        #region Tout transférer.

        /// <summary>
        /// Permet de transférer toute la liste ListDGVListNema dans ListDGVListNemaAdd.
        /// </summary>
        /// <remarks>
        /// Cette méthode permet aussi de vider DGVListNema et de remplir DGVListNemaAdd.
        /// </remarks>
        /// <param name="sender">Nous n'utiliserons pas ce paramètre.</param>
        /// <param name="e">Nous n'utiliserons pas ce paramètre.</param>
        /// <returns>Ne retourne aucune valeur.</returns>
        private void BAddTransfertAll_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < ListDGVListNema.Count; i++)
            {
                DGVListNemaAdd.Rows.Add(ListDGVListNema[i]);
                ListDGVListNemaAdd.Add(ListDGVListNema[i]);
                ListDGVListNema.RemoveAt(i);
                i = i - 1;
            }
            DGVListNema.Rows.Clear();
        }

        /// <summary>
        /// Permet de transférer toute la liste ListDGVListNemaAdd dans ListDGVListNema.
        /// </summary>
        /// <remarks>
        /// Cette méthode permet aussi de vider DGVListNemaAdd et de remplir DGVListNema.
        /// </remarks>
        /// <param name="sender">Nous n'utiliserons pas ce paramètre.</param>
        /// <param name="e">Nous n'utiliserons pas ce paramètre.</param>
        /// <returns>Ne retourne aucune valeur.</returns>
        private void BCancelTransfertAll_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < ListDGVListNemaAdd.Count; i++)
            {
                DGVListNema.Rows.Add(ListDGVListNemaAdd[i]);
                ListDGVListNema.Add(ListDGVListNemaAdd[i]);
                ListDGVListNemaAdd.RemoveAt(i);
                i = i - 1;
            }
            DGVListNemaAdd.Rows.Clear();
        }
        #endregion

        #region transférer un élément.
        /// <summary>
        /// Permet de transférer un élément la liste ListDGVListNema dans ListDGVListNemaAdd.
        /// </summary>
        /// <remarks>
        /// Cette méthode permet aussi de supprimer l'élément selectionné dans DGVListNema et de l'ajouter dans DGVListNemaAdd.
        /// Si aucun élément n'est selectionné, on prend par défaut la première valeure de ListDGVListNema.
        /// Si ListDGVListNema est vide, il ne se passe rien lorsque l'on clique sur le bouton.
        /// </remarks>
        /// <param name="sender">Nous n'utiliserons pas ce paramètre.</param>
        /// <param name="e">Nous n'utiliserons pas ce paramètre.</param>
        /// <returns>Ne retourne aucune valeur.</returns>
        private void BAddTransfert_Click(object sender, EventArgs e)
        {

            if (ClickedNema != "" && ClickedNema != Pas2fois)
            {
                ListDGVListNemaAdd.Add(ClickedNema);
                ListDGVListNema.Remove(ClickedNema);
                DGVListNemaAdd.Rows.Add(ListDGVListNemaAdd[ListDGVListNemaAdd.Count - 1]);
                DGVListNema.Rows.Clear();
                DGVListNemaAdd.Rows.Clear();
                for (int i = 0; i < ListDGVListNema.Count; i++)
                {
                    DGVListNema.Rows.Add(ListDGVListNema[i]);
                }
                for (int i = 0; i < ListDGVListNemaAdd.Count; i++)
                {
                    DGVListNemaAdd.Rows.Add(ListDGVListNemaAdd[i]);
                }
                Pas2fois = ClickedNema;
                Pas2foisAdd = "";
                ClickedNemaAdd = "";
            }
            else if (ClickedNema == "" && ListDGVListNema.Count != 0) // Si on a selectionné aucune nématode, cela prend la première de la liste
            {
                ListDGVListNemaAdd.Add(ListDGVListNema[0]);
                ListDGVListNema.Remove(ListDGVListNema[0]);
                DGVListNemaAdd.Rows.Add(ListDGVListNemaAdd[ListDGVListNemaAdd.Count - 1]); // ?
                DGVListNema.Rows.Clear();
                DGVListNemaAdd.Rows.Clear();
                for (int i = 0; i < ListDGVListNema.Count; i++)
                {
                    DGVListNema.Rows.Add(ListDGVListNema[i]);
                }
                for (int i = 0; i < ListDGVListNemaAdd.Count; i++)
                {
                    DGVListNemaAdd.Rows.Add(ListDGVListNemaAdd[i]);
                }
                Pas2fois = ClickedNema;
                Pas2foisAdd = "";
                ClickedNemaAdd = "";
            }
            else if (ListDGVListNema.Count == 0) { }
        }

        /// <summary>
        /// Permet de transférer un élément la liste ListDGVListNemaAdd dans ListDGVListNema.
        /// </summary>
        /// <remarks>
        /// Cette méthode permet aussi de supprimer l'élément selectionné dans DGVListNemaAdd et de l'ajouter dans DGVListNema.
        /// Si aucun élément n'est selectionné, on prend par défaut la première valeure de ListDGVListNemaAdd.
        /// Si ListDGVListNemaAdd est vide, il ne se passe rien lorsque l'on clique sur le bouton.
        /// </remarks>
        /// <param name="sender">Nous n'utiliserons pas ce paramètre.</param>
        /// <param name="e">Nous n'utiliserons pas ce paramètre.</param>
        /// <returns>Ne retourne aucune valeur.</returns>
        private void BCancelTransfert_Click(object sender, EventArgs e)
        {
            if (ClickedNemaAdd != "" && ClickedNemaAdd != Pas2foisAdd)
            {
                ListDGVListNema.Add(ClickedNemaAdd);
                ListDGVListNemaAdd.Remove(ClickedNemaAdd);
                DGVListNema.Rows.Clear();
                DGVListNemaAdd.Rows.Clear();
                for (int i = 0; i < ListDGVListNema.Count; i++)
                {
                    DGVListNema.Rows.Add(ListDGVListNema[i]);
                }
                for (int i = 0; i < ListDGVListNemaAdd.Count; i++)
                {
                    DGVListNemaAdd.Rows.Add(ListDGVListNemaAdd[i]);
                }
                Pas2foisAdd = ClickedNemaAdd;
                Pas2fois = "";
                ClickedNema = "";
            }
            else if (ClickedNemaAdd == "" && ListDGVListNemaAdd.Count != 0) // Si on a selectionné aucune nématode, cela prend la première de la liste
            {
                ListDGVListNema.Add(ListDGVListNemaAdd[0]);
                ListDGVListNemaAdd.Remove(ListDGVListNemaAdd[0]);
                DGVListNema.Rows.Clear();
                DGVListNemaAdd.Rows.Clear();
                for (int i = 0; i < ListDGVListNema.Count; i++)
                {
                    DGVListNema.Rows.Add(ListDGVListNema[i]);
                }
                for (int i = 0; i < ListDGVListNemaAdd.Count; i++)
                {
                    DGVListNemaAdd.Rows.Add(ListDGVListNemaAdd[i]);
                }
                Pas2foisAdd = ClickedNemaAdd;
                Pas2fois = "";
                ClickedNema = "";
            }
            else if (ListDGVListNemaAdd.Count == 0) { }
        }
        #endregion

        #region Selectionner une valeure d'un DataGridView.
        /// <summary>
        /// Récupère la valeure de l'élément selectionné dans DGVListNemaAdd.
        /// </summary>
        /// <remarks>
        /// On ajoute la valeure selectionnée dans la variable ClickedNemaAdd.
        /// </remarks>
        /// <param name="sender">Nous n'utiliserons pas ce paramètre.</param>
        /// <param name="e">DataGridViewCellEventArgs, nous l'utilisons pour récupérer les valeures d'une cellule.</param>
        /// <returns>Ne retourne aucune valeur.</returns>
        private void DGVListNemaAdd_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                ClickedNemaAdd = DGVListNemaAdd.Rows[e.RowIndex].Cells[0].Value.ToString();
            }
        }

        /// <summary>
        /// Récupère la valeure de l'élément selectionné dans DGVListNema.
        /// </summary>
        /// <remarks>
        /// On ajoute la valeure selectionnée dans la variable ClickedNema.
        /// </remarks>
        /// <param name="sender">Nous n'utiliserons pas ce paramètre.</param>
        /// <param name="e">DataGridViewCellEventArgs, nous l'utilisons pour récupérer les valeures d'une cellule.</param>
        /// <returns>Ne retourne aucune valeur.</returns>
        private void DGVListNema_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                ClickedNema = DGVListNema.Rows[e.RowIndex].Cells[0].Value.ToString();
            }
        }
        #endregion

        private void BAnnuler_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void tbRecherche_TextChanged(object sender, EventArgs e)
        {
            using (db_entities context = new db_entities())
            {
                //var nema = context.tNematodes.FirstOrDefault(n => n.cNom_Complet.Contains(tbRecherche.Text));
                //label1.Text = nema.cNom_Complet;
            }
        }

        private void BValider_Click(object sender, EventArgs e)
        {
            using (db_entities c = new db_entities())
            {
                if (up == "")
                {
                    var ListeReduite = new t_liste_reduite() { cnom = tbNomListe.Text };
                    c.t_liste_reduite.Add(ListeReduite);
                    c.SaveChanges();
                    foreach (var i in ListDGVListNemaAdd)
                    {
                        var nema = c.t_nematodes.FirstOrDefault(n => n.cnom_complet == i);
                        //nema.tListeReduiteNema.Add(new tListeReduiteNema() { iListe = ListeReduite.iID_ListeReduite} ); ==> Ne fonctionne pas car 2 PK ou 2 FK ??
                        var listereduitenema = new t_liste_reduite_nematode() { ilistereduite = ListeReduite.id, inematodes = nema.id };
                        c.t_liste_reduite_nematode.Add(listereduitenema);
                        c.SaveChanges();
                    }
                }
                else
                {

                    //faire l'update
                }              
            }
        }
    }
}
