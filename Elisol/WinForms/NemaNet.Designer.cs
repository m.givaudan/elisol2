﻿namespace Elisol
{
    partial class NemaNet
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bEchantillon = new System.Windows.Forms.Button();
            this.cbEchantillon = new System.Windows.Forms.ComboBox();
            this.bDossier = new System.Windows.Forms.Button();
            this.cbDossier = new System.Windows.Forms.ComboBox();
            this.dgvNematodes = new System.Windows.Forms.DataGridView();
            this.Nematode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantité = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bClient = new System.Windows.Forms.Button();
            this.cbClient = new System.Windows.Forms.ComboBox();
            this.bPlay = new System.Windows.Forms.Button();
            this.bAddList = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.bValiderListe = new System.Windows.Forms.Button();
            this.BUpdateList = new System.Windows.Forms.Button();
            this.cbListReduiteNema = new System.Windows.Forms.ComboBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.gbClient = new System.Windows.Forms.GroupBox();
            this.gbDossier = new System.Windows.Forms.GroupBox();
            this.gbEchantillon = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNematodes)).BeginInit();
            this.gbClient.SuspendLayout();
            this.gbDossier.SuspendLayout();
            this.gbEchantillon.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bEchantillon
            // 
            this.bEchantillon.Location = new System.Drawing.Point(287, 23);
            this.bEchantillon.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bEchantillon.Name = "bEchantillon";
            this.bEchantillon.Size = new System.Drawing.Size(53, 28);
            this.bEchantillon.TabIndex = 11;
            this.bEchantillon.Text = "...";
            this.bEchantillon.UseVisualStyleBackColor = true;
            this.bEchantillon.Click += new System.EventHandler(this.bEchantillon_Click);
            // 
            // cbEchantillon
            // 
            this.cbEchantillon.FormattingEnabled = true;
            this.cbEchantillon.Location = new System.Drawing.Point(8, 23);
            this.cbEchantillon.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbEchantillon.Name = "cbEchantillon";
            this.cbEchantillon.Size = new System.Drawing.Size(271, 24);
            this.cbEchantillon.TabIndex = 10;
            this.cbEchantillon.SelectionChangeCommitted += new System.EventHandler(this.cbEchantillon_SelectionChangeCommitted);
            // 
            // bDossier
            // 
            this.bDossier.Location = new System.Drawing.Point(287, 21);
            this.bDossier.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bDossier.Name = "bDossier";
            this.bDossier.Size = new System.Drawing.Size(53, 28);
            this.bDossier.TabIndex = 9;
            this.bDossier.Text = "...";
            this.bDossier.UseVisualStyleBackColor = true;
            this.bDossier.Click += new System.EventHandler(this.bDossier_Click);
            // 
            // cbDossier
            // 
            this.cbDossier.FormattingEnabled = true;
            this.cbDossier.Location = new System.Drawing.Point(8, 23);
            this.cbDossier.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbDossier.Name = "cbDossier";
            this.cbDossier.Size = new System.Drawing.Size(271, 24);
            this.cbDossier.TabIndex = 8;
            this.cbDossier.SelectionChangeCommitted += new System.EventHandler(this.cbDossier_SelectionChangeCommitted);
            // 
            // dgvNematodes
            // 
            this.dgvNematodes.AllowUserToAddRows = false;
            this.dgvNematodes.AllowUserToDeleteRows = false;
            this.dgvNematodes.AllowUserToResizeColumns = false;
            this.dgvNematodes.AllowUserToResizeRows = false;
            this.dgvNematodes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvNematodes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Nematode,
            this.Quantité});
            this.dgvNematodes.EnableHeadersVisualStyles = false;
            this.dgvNematodes.Location = new System.Drawing.Point(8, 59);
            this.dgvNematodes.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvNematodes.MultiSelect = false;
            this.dgvNematodes.Name = "dgvNematodes";
            this.dgvNematodes.ReadOnly = true;
            this.dgvNematodes.RowHeadersVisible = false;
            this.dgvNematodes.Size = new System.Drawing.Size(281, 174);
            this.dgvNematodes.TabIndex = 7;
            // 
            // Nematode
            // 
            this.Nematode.HeaderText = "Nematode";
            this.Nematode.Name = "Nematode";
            this.Nematode.ReadOnly = true;
            // 
            // Quantité
            // 
            this.Quantité.HeaderText = "Quantité";
            this.Quantité.Name = "Quantité";
            this.Quantité.ReadOnly = true;
            // 
            // bClient
            // 
            this.bClient.Location = new System.Drawing.Point(287, 22);
            this.bClient.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bClient.Name = "bClient";
            this.bClient.Size = new System.Drawing.Size(53, 28);
            this.bClient.TabIndex = 13;
            this.bClient.Text = "...";
            this.bClient.UseVisualStyleBackColor = true;
            this.bClient.Click += new System.EventHandler(this.bClient_Click);
            // 
            // cbClient
            // 
            this.cbClient.FormattingEnabled = true;
            this.cbClient.Location = new System.Drawing.Point(8, 23);
            this.cbClient.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbClient.Name = "cbClient";
            this.cbClient.Size = new System.Drawing.Size(271, 24);
            this.cbClient.TabIndex = 12;
            this.cbClient.SelectedIndexChanged += new System.EventHandler(this.cbClient_SelectedIndexChanged);
            this.cbClient.SelectionChangeCommitted += new System.EventHandler(this.cbClient_SelectionChangeCommitted);
            // 
            // bPlay
            // 
            this.bPlay.Location = new System.Drawing.Point(311, 59);
            this.bPlay.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bPlay.Name = "bPlay";
            this.bPlay.Size = new System.Drawing.Size(255, 90);
            this.bPlay.TabIndex = 14;
            this.bPlay.Text = "PLAY";
            this.bPlay.UseVisualStyleBackColor = true;
            this.bPlay.Click += new System.EventHandler(this.bPlay_Click);
            // 
            // bAddList
            // 
            this.bAddList.Location = new System.Drawing.Point(7, 79);
            this.bAddList.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bAddList.Name = "bAddList";
            this.bAddList.Size = new System.Drawing.Size(120, 42);
            this.bAddList.TabIndex = 17;
            this.bAddList.Text = "Créer une liste";
            this.bAddList.UseVisualStyleBackColor = true;
            this.bAddList.Click += new System.EventHandler(this.bAddList_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 23);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(240, 17);
            this.label1.TabIndex = 18;
            this.label1.Text = "Selectionnez une liste de nématodes";
            // 
            // bValiderListe
            // 
            this.bValiderListe.Location = new System.Drawing.Point(7, 128);
            this.bValiderListe.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bValiderListe.Name = "bValiderListe";
            this.bValiderListe.Size = new System.Drawing.Size(255, 58);
            this.bValiderListe.TabIndex = 19;
            this.bValiderListe.Text = "Valider la liste";
            this.bValiderListe.UseVisualStyleBackColor = true;
            this.bValiderListe.Click += new System.EventHandler(this.bValiderListe_Click);
            // 
            // BUpdateList
            // 
            this.BUpdateList.Location = new System.Drawing.Point(141, 79);
            this.BUpdateList.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BUpdateList.Name = "BUpdateList";
            this.BUpdateList.Size = new System.Drawing.Size(120, 42);
            this.BUpdateList.TabIndex = 24;
            this.BUpdateList.Text = "Modifier la liste";
            this.BUpdateList.UseVisualStyleBackColor = true;
            this.BUpdateList.Click += new System.EventHandler(this.BUpdateList_Click);
            // 
            // cbListReduiteNema
            // 
            this.cbListReduiteNema.FormattingEnabled = true;
            this.cbListReduiteNema.Location = new System.Drawing.Point(7, 46);
            this.cbListReduiteNema.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbListReduiteNema.Name = "cbListReduiteNema";
            this.cbListReduiteNema.Size = new System.Drawing.Size(253, 24);
            this.cbListReduiteNema.TabIndex = 26;
            this.cbListReduiteNema.SelectionChangeCommitted += new System.EventHandler(this.cbListReduiteNema_SelectionChangeCommitted);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // gbClient
            // 
            this.gbClient.Controls.Add(this.cbClient);
            this.gbClient.Controls.Add(this.bClient);
            this.gbClient.Location = new System.Drawing.Point(16, 11);
            this.gbClient.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbClient.Name = "gbClient";
            this.gbClient.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbClient.Size = new System.Drawing.Size(348, 57);
            this.gbClient.TabIndex = 27;
            this.gbClient.TabStop = false;
            this.gbClient.Text = "Client";
            // 
            // gbDossier
            // 
            this.gbDossier.Controls.Add(this.cbDossier);
            this.gbDossier.Controls.Add(this.bDossier);
            this.gbDossier.Location = new System.Drawing.Point(16, 75);
            this.gbDossier.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbDossier.Name = "gbDossier";
            this.gbDossier.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbDossier.Size = new System.Drawing.Size(348, 57);
            this.gbDossier.TabIndex = 28;
            this.gbDossier.TabStop = false;
            this.gbDossier.Text = "Dossier";
            // 
            // gbEchantillon
            // 
            this.gbEchantillon.Controls.Add(this.cbEchantillon);
            this.gbEchantillon.Controls.Add(this.bEchantillon);
            this.gbEchantillon.Controls.Add(this.dgvNematodes);
            this.gbEchantillon.Controls.Add(this.bPlay);
            this.gbEchantillon.Location = new System.Drawing.Point(16, 139);
            this.gbEchantillon.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbEchantillon.Name = "gbEchantillon";
            this.gbEchantillon.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbEchantillon.Size = new System.Drawing.Size(348, 58);
            this.gbEchantillon.TabIndex = 28;
            this.gbEchantillon.TabStop = false;
            this.gbEchantillon.Text = "Echantillon";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.bAddList);
            this.groupBox1.Controls.Add(this.bValiderListe);
            this.groupBox1.Controls.Add(this.BUpdateList);
            this.groupBox1.Controls.Add(this.cbListReduiteNema);
            this.groupBox1.Location = new System.Drawing.Point(405, 15);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(277, 198);
            this.groupBox1.TabIndex = 29;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Client";
            // 
            // NemaNet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(380, 209);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbEchantillon);
            this.Controls.Add(this.gbDossier);
            this.Controls.Add(this.gbClient);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "NemaNet";
            this.Text = "NemaNet";
            this.Load += new System.EventHandler(this.NemaNet_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvNematodes)).EndInit();
            this.gbClient.ResumeLayout(false);
            this.gbDossier.ResumeLayout(false);
            this.gbEchantillon.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bEchantillon;
        private System.Windows.Forms.ComboBox cbEchantillon;
        private System.Windows.Forms.Button bDossier;
        private System.Windows.Forms.ComboBox cbDossier;
        private System.Windows.Forms.DataGridView dgvNematodes;
        private System.Windows.Forms.Button bClient;
        private System.Windows.Forms.ComboBox cbClient;
       // private elisolDataSetTableAdapters.tEchantillonsTableAdapter tEchantillonsTableAdapter;
        private System.Windows.Forms.Button bPlay;
        private System.Windows.Forms.Button bAddList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bValiderListe;
        private System.Windows.Forms.Button BUpdateList;
        private System.Windows.Forms.ComboBox cbListReduiteNema;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.GroupBox gbClient;
        private System.Windows.Forms.GroupBox gbDossier;
        private System.Windows.Forms.GroupBox gbEchantillon;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nematode;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantité;
        //  private elisolDataSetTableAdapters.tClientsTableAdapter tClientsTableAdapter1;
        // private elisolDataSet elisolDataSet2;
    }
}

