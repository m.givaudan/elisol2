﻿using Elisol2.Entity_Data_Models.Elisol___PostgreSQL_NPGSQL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Elisol2
{
    public partial class Nouveau : Form
    {
        string type;
        string NomListNema;
        tClients client;
        tDossiers dossier;
        tListeReduite LR;

        public Nouveau(string s)
        {
            InitializeComponent();
            type = s;

            #region IHM d'un client
            gbDossier.Hide();
            gbEchantillon.Hide();

            gbClient.Show();
            gbClient.Location = new Point(15, 15);

            bValider.Location = new Point(gbClient.Location.X, tbNum_Siren.Bottom + 40);
            bAnnuler.Location = new Point(bValider.Location.X + 120, tbNum_Siren.Bottom + 40);

            Size = new Size(gbClient.Right + 30, bValider.Bottom + 50);
            #endregion   
        }
        
        public Nouveau(string s, tClients c)
        {
            InitializeComponent();
            type = s;
            client = c;

            #region IHM d'un dossier
            gbClient.Hide();
            gbEchantillon.Hide();

            gbDossier.Show();
            gbDossier.Location = new Point(15, 15);

            bValider.Location = new Point(gbDossier.Location.X, tbNom_Dossier.Bottom + 40);
            bAnnuler.Location = new Point(bValider.Location.X + 120, tbNom_Dossier.Bottom + 40);

            Size = new Size(gbDossier.Right + 30, bValider.Bottom + 50);
            #endregion
        }

        public Nouveau(string s, tDossiers d)
        {
            InitializeComponent();
            type = s;
            dossier = d;

            #region IHM d'un echantillon
            using (var context = new db_entities())
            {
                var ListeReduite = context.tListeReduite.ToList();
                cbListReduiteNema.DataSource = ListeReduite;
                cbListReduiteNema.SelectedIndex = -1;
                cbListReduiteNema.Text = "Selectionnez une liste réduite";
            }

            gbClient.Hide();
            gbDossier.Hide();

            gbEchantillon.Show();
            gbEchantillon.Location = new Point(15, 15);

            bValider.Location = new Point(gbEchantillon.Location.X, bAddList.Bottom + 40);
            bAnnuler.Location = new Point(bValider.Location.X + 120, bAddList.Bottom + 40);

            Size = new Size(gbEchantillon.Right + 30, bValider.Bottom + 50);
            #endregion
        }


        private void bAnnuler_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void bValider_Click(object sender, EventArgs e)
        {
            using (var c = new db_entities())
            {
                string info = "Voulez-vous vraiment créer ";

                if (type == "tClients")
                {
                    #region Création d'un client
                    c.tClients.Add(new tClients()
                    {
                        cAdresse = tbAdresse.Text,
                        cNom_Entreprise = tbNom_Entreprise.Text,
                        cNum_Tel = tbNum_Tel.Text,
                        iNum_Commande = Convert.ToInt32(tbNum_Commande.Text),
                        iCode_Postal = Convert.ToInt32(tbCode_Postal.Text),
                        iNum_Siren = Convert.ToInt32(tbNum_Siren.Text)
                    });
                    info = info + tbNom_Entreprise.Text;
                    #endregion
                }
                if (type == "tDossiers")
                {
                    #region Création d'un dossier
                    c.tDossiers.Add(new tDossiers()
                    {
                        cNom = tbNom_Dossier.Text,
                        iClient = client.iID_Clients
                    });
                    info = info + tbNom_Dossier.Text;
                    #endregion
                }
                if (type == "tEchantillons")
                {
                    #region Création d'un echantillon

                    c.tEchantillons.Add(new tEchantillons()
                    {
                        cNom = tbNom_Echantillon.Text,
                        iDossier = dossier.iID_Dossiers,
                        iListe = LR.iID_ListeReduite
                    });
                    info = info + tbNom_Echantillon.Text;
                    #endregion
                }

                if (MessageBox.Show(this, info, "!! Warning !!", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    c.SaveChanges();
                    Close();
                }
            }           
        }

        private void cbListReduiteNema_SelectionChangeCommitted(object sender, EventArgs e)
        {
            NomListNema = cbListReduiteNema.SelectedValue.ToString();
            if (NomListNema != "Selectionnez une liste réduite")
            {
                using (var c = new db_entities())
                {
                    LR = c.tListeReduite.FirstOrDefault(lr => lr.cNom == cbListReduiteNema.SelectedValue.ToString());
                }
                bValider.Enabled = true;
            }
        }
    }
}
