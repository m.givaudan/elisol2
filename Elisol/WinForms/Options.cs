﻿using Elisol.Entity_Data_Models.ElisolModels;
using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Elisol
{
    public partial class Options : Form //Options<T>: Form
    {
        t_clients clients;
        t_dossiers dossier;
        t_echantillons echantillon;
        string type;

       

        public Options(string s)
        {
            InitializeComponent();
            bModifier.Enabled = false;
            bSupprimer.Enabled = false;
            type = s;
        }

        public Options(t_clients c)
        {
            InitializeComponent();
            Text = c.cnom_entreprise;
            clients = c;
            type = "t_clients";
        }

        public Options(t_dossiers d, t_clients c)
        {
            InitializeComponent();
            Text = d.cnom;
            dossier = d;
            clients = c;
            type = "t_dossiers";
        }

        public Options(string s, t_clients c)
        {
            InitializeComponent();
            clients = c;
            type = s;
            bModifier.Enabled = false;
            bSupprimer.Enabled = false;
        }

        public Options(t_echantillons e, t_dossiers d)
        {
            InitializeComponent();
            Text = e.cnom;
            echantillon = e;
            dossier = d;
            type = "t_echantillons";
        }

        public Options(string s, t_dossiers d)
        {
            InitializeComponent();
            dossier = d;
            type = s;
            bModifier.Enabled = false;
            bSupprimer.Enabled = false;
        }

        private void bModifier_Click(object sender, EventArgs e)
        {
            if (type == "t_clients")
            {
                Modifier IHMclientModifier = new Modifier(clients);
                IHMclientModifier.Show();
            }
            else if(type == "t_dossiers")
            {
                Modifier IHMdossierModifier = new Modifier(dossier);
                IHMdossierModifier.Show();
            }
            else if(type == "t_echantillons")
            {
                Modifier IHMechantillonModifier = new Modifier(echantillon);
                IHMechantillonModifier.Show();
            }
            Close();
        }

        private void bNouveau_Click(object sender, EventArgs e)
        {
            if (type == "t_clients")
            {
                Modifier IHMNouveau = new Modifier("t_clients");
                IHMNouveau.Show();
            }
            else if (type == "t_dossiers")
            {
                Modifier IHMNouveau = new Modifier("t_dossiers", clients);
                IHMNouveau.Show();
            }
            else if (type == "t_echantillons")
            {
                Modifier IHMNouveau = new Modifier("t_echantillons", dossier);
                IHMNouveau.Show();
            }
            Close();
        }

        private void bSupprimer_Click(object sender, EventArgs e)
        {
            using (var c = new db_entities())
            {
                if (type == "t_clients" && 
                    MessageBox.Show(this, "Etes-vous sûr de vouloir supprimer ce client ?", 
                    "!! Warning !!", 
                    MessageBoxButtons.YesNo, 
                    MessageBoxIcon.Warning, 
                    MessageBoxDefaultButton.Button2) 
                    == DialogResult.Yes)
                {
                    c.Entry(clients).State = System.Data.Entity.EntityState.Deleted;
                    c.SaveChanges();
                    MessageBox.Show("la suppression a bien été effectuée");
                    Close();
                }
                else if (type == "t_dossiers" &&
                    MessageBox.Show(this, "Etes-vous sûr de vouloir supprimer ce dossier ?", "!! Warning !!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2)
                    == DialogResult.Yes)
                {
                    c.Entry(dossier).State = System.Data.Entity.EntityState.Deleted;
                    c.SaveChanges();
                    MessageBox.Show("la suppression a bien été effectuée");
                    Close();
                }
                else if (type == "t_echantillons" &&
                    MessageBox.Show(this, "Etes-vous sûr de vouloir supprimer cet échantillon ?", "!! Warning !!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2)
                    == DialogResult.Yes)
                {
                    c.Entry(echantillon).State = System.Data.Entity.EntityState.Deleted;
                    c.SaveChanges();
                    MessageBox.Show("la suppression a bien été effectuée");
                    Close();
                }
            }   
        }
    }
}
