﻿using Elisol.Entity_Data_Models.ElisolModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Media;
using System.Speech.Recognition;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Elisol
{
    public partial class NemaNet : Form
    {
        List<t_clients> ListClients;
        t_clients thisClient;
        string nomEntreprise;

        List<t_dossiers> ListDossier;
        t_dossiers thisDossier;
        string nomDossier;

        List<t_echantillons> ListEchantillon;
        t_echantillons thisEchantillon;
        string nomEchantillon;

        string NomListNema;

        bool client;
        bool dossier;
        bool echantillon;

        List<t_nematodes> nemaList = new List<t_nematodes>();
        List<CSVDictee> mesDictees = new List<CSVDictee>();

        public NemaNet()
        {
            InitializeComponent();
            #region à supprimer plus tard
            groupBox1.Hide();

            BUpdateList.Enabled = false;
            bValiderListe.Enabled = false;
            #endregion

            #region Connecté
            using (db_entities context = new db_entities())                     // Utilise un objet de connection à la BDD
            {
                //context.t_nematodes.Add(new t_nematodes() { cnom_court = "rouge", cnom_complet = "rouge"});
                //context.t_nematodes.Add(new t_nematodes() { cnom_court = "bleu", cnom_complet = "bleu" });
                //context.t_nematodes.Add(new t_nematodes() { cnom_court = "vert", cnom_complet = "vert" });
                //context.SaveChanges();

                ListClients = context.t_clients.ToList();
                //cbClient.DataSource = ListClients;
                foreach (var item in ListClients)
                {
                    cbClient.Items.Add(item.cnom_entreprise);
                }// Ajoute en données dans la ComboBox une liste de clients

                cbClient.SelectedIndex = -1;
                cbClient.Text = "Selectionnez une entreprise";
                

                //var ListeReduite = context.t_liste_reduite.ToList();
                //cbListReduiteNema.DataSource = ListeReduite;
                //cbListReduiteNema.SelectedIndex = -1;
                //cbListReduiteNema.Text = "Selectionnez une liste réduite";
            }
            #endregion
        }
     
        #region Affiche les clients puis les dossiers, echantillons et nématodes en fonction des selections.
        /// <summary>
        /// Affiche les Dossiers correspondant au client selectionné.
        /// </summary>
        /// <remarks>
        /// Cette méthode remplis cbDossier qui sont en lien avec un client lors d'une selection.
        /// </remarks>
        /// <param name="sender">Nous n'utiliserons pas ce paramètre.</param>
        /// <param name="e">Nous n'utiliserons pas ce paramètre.</param>
        /// <returns>Ne retourne aucune valeur.</returns>
        private void cbClient_SelectionChangeCommitted(object sender, EventArgs e)
        {
            cbDossier.Items.Clear();
            using (db_entities context = new db_entities())
            {
                nomEntreprise = cbClient.SelectedItem.ToString();
                //nomEntreprise = cbClient.SelectedValue.ToString();
                thisClient = context.t_clients.FirstOrDefault(c => c.cnom_entreprise == nomEntreprise);
                ListDossier = thisClient.t_dossiers.ToList();
                //cbDossier.DataSource = thisClient.t_dossiers.ToList();
                foreach (var item in ListDossier)
                {
                    cbDossier.Items.Add(item.cnom);
                }
                if (cbClient.SelectedItem.ToString() == "Selectionnez une entreprise") client = false;
                else client = true;

                if (ListDossier.Count == 0)
                {
                    cbDossier.SelectedIndex = -1;
                    cbDossier.Text = "Selectionnez un dossier"; 
                }
                else
                {
                    for (int i = 0; i < ListDossier.Count; i++)
                    {
                        cbDossier.DisplayMember = ListDossier[i].cnom;              // Affiche cnom pour chaque dossiers de la liste
                    }
                    cbDossier.SelectedIndex = -1;
                    cbDossier.Text = "Selectionnez un dossier";
                }
            }
        }

        /// <summary>
        /// Affiche les Echantillons correspondant au dossier selectionné.
        /// </summary>
        /// <remarks>
        /// Cette méthode remplis cbEchantillon qui sont en lien avec un dossier lors d'une selection.
        /// </remarks>
        /// <param name="sender">Nous n'utiliserons pas ce paramètre.</param>
        /// <param name="e">Nous n'utiliserons pas ce paramètre.</param>
        /// <returns>Ne retourne aucune valeur.</returns>
        private void cbDossier_SelectionChangeCommitted(object sender, EventArgs e)
        {
            cbEchantillon.Items.Clear();
            using (db_entities context = new db_entities())
            {
                nomDossier = cbDossier.SelectedItem.ToString();
                thisDossier = context.t_dossiers.FirstOrDefault(c => c.cnom == nomDossier);
                ListEchantillon = thisDossier.t_echantillons.ToList();
                //cbEchantillon.DataSource = ListEchantillon;
                foreach (var item in ListEchantillon)
                {
                    cbEchantillon.Items.Add(item.cnom);
                }

                if (cbDossier.SelectedItem.ToString() == "Selectionnez un dossier") dossier = false;
                else dossier = true;

                if (ListEchantillon.Count == 0)
                {
                    cbEchantillon.SelectedIndex = -1;
                    cbEchantillon.Text = "Selectionnez un echantillon";
                }
                else
                {
                    for (int i = 0; i < ListEchantillon.Count; i++)
                    {
                        cbEchantillon.DisplayMember = ListEchantillon[i].cnom;      // Affiche cnom pour chaque echantillons de la liste
                    }
                    cbEchantillon.SelectedIndex = -1;
                    cbEchantillon.Text = "Selectionnez un echantillon";
                    dossier = true;
                }
            }
        }

        /// <summary>
        /// Affiche les nematodes et les quantitées correspondant à l'échantillon selectionné.
        /// </summary>
        /// <remarks>
        /// Cette méthode remplis dgvNema qui sont en lien avec un échantillon lors d'une selection.
        /// </remarks>
        /// <param name="sender">Nous n'utiliserons pas ce paramètre.</param>
        /// <param name="e">Nous n'utiliserons pas ce paramètre.</param>
        /// <returns>Ne retourne aucune valeur.</returns>
        private void cbEchantillon_SelectionChangeCommitted(object sender, EventArgs e)
        {

            #region Connecté
            using (db_entities context = new db_entities())
            {
                nomEchantillon = cbEchantillon.SelectedItem.ToString();
                thisEchantillon = context.t_echantillons.FirstOrDefault(c => c.cnom == nomEchantillon);

                if (cbEchantillon.SelectedItem.ToString() == "Selectionnez un echantillon") echantillon = false;
                else echantillon = true;

            }
            chargerNematode();
            #endregion
        }

        /// <summary>
        /// Récupère le nom de la liste réduite
        /// </summary>
        /// <remarks>
        /// Cette méthode récupère la valeure de l'objet selectionné.
        /// </remarks>
        /// <param name="sender">Nous n'utiliserons pas ce paramètre.</param>
        /// <param name="e">Nous n'utiliserons pas ce paramètre.</param>
        /// <returns>Ne retourne aucune valeur.</returns>
        private void cbListReduiteNema_SelectionChangeCommitted(object sender, EventArgs e)
        {
            NomListNema = cbListReduiteNema.SelectedValue.ToString();
            if (NomListNema != "")
            {
                BUpdateList.Enabled = true;
                bValiderListe.Enabled = true;
            }  
        }
        #endregion

        #region Afficher les options
        /// <summary>
        /// Permet de lancer la WinFrom Option d'un Client.
        /// </summary>
        /// <remarks>
        /// On récupère le type (1,2,3) pour savoir quelle option (Client, Dossier, Echantillon) nous allons modifier.
        /// Ici, ce sera le Client.
        /// On choisis un client, et on récupère son Index (id) pour modifier celui selectionné.
        /// </remarks>
        /// <param name="sender">Nous n'utiliserons pas ce paramètre.</param>
        /// <param name="e">Nous n'utiliserons pas ce paramètre.</param>
        /// <returns>Ne retourne aucune valeur.</returns>
        private void bClient_Click(object sender, EventArgs e)
        {
            if (client == true)
            {
                Options mOptClient = new Options(thisClient);
                mOptClient.Show();
            }
            else
            {
                Options mOptClient = new Options("t_clients");
                mOptClient.Show();
            }
        }

        /// <summary>
        /// Permet de lancer la WinFrom Option d'un Dossier.
        /// </summary>
        /// <remarks>
        /// On récupère le type (1,2,3) pour savoir quelle option (Client, Dossier, Echantillon) nous allons modifier.
        /// Ici, ce sera le Dossier.
        /// On choisis un dossier, et on récupère son Index (id) pour modifier celui selectionné.
        /// </remarks>
        /// <param name="sender">Nous n'utiliserons pas ce paramètre.</param>
        /// <param name="e">Nous n'utiliserons pas ce paramètre.</param>
        /// <returns>Ne retourne aucune valeur.</returns>
        private void bDossier_Click(object sender, EventArgs e)
        {
            if (dossier == true)
            {
                Options mOptDossier = new Options(thisDossier, thisClient);
                mOptDossier.Show();
            }
            else
            {
                Options mOptDossier = new Options("t_dossiers", thisClient);
                mOptDossier.Show();
            }
        }

        /// <summary>
        /// Permet de lancer la WinFrom Option d'un Echantillon.
        /// </summary>
        /// <remarks>
        /// On récupère le type (1,2,3) pour savoir quelle option (Client, Dossier, Echantillon) nous allons modifier.
        /// Ici, ce sera un Echantillon.
        /// On choisis un echantillon, et on récupère son Index (id) pour modifier celui selectionné.
        /// </remarks>
        /// <param name="sender">Nous n'utiliserons pas ce paramètre.</param>
        /// <param name="e">Nous n'utiliserons pas ce paramètre.</param>
        /// <returns>Ne retourne aucune valeur.</returns>
        private void bEchantillon_Click(object sender, EventArgs e)
        {
            if (echantillon == true)
            {
                Options mOptEchantillon = new Options(thisEchantillon, thisDossier);
                mOptEchantillon.Show();
            }
            else
            {
                Options mOptEchantillon = new Options("t_echantillons", thisDossier);
                mOptEchantillon.Show();
            }
        }
        #endregion

        private void BUpdateList_Click(object sender, EventArgs e)
        {            
            using (var c = new db_entities())
            {
                //List<t_liste_reduite_nematode> ListeReduiteNema = c.t_liste_reduite.FirstOrDefault(lr => lr.cnom == cbListReduiteNema.SelectedValue.ToString()).t_liste_reduite_nematode.ToList();

                t_liste_reduite ListeReduite = c.t_liste_reduite.FirstOrDefault(lr => lr.cnom == NomListNema);
                int id = ListeReduite.id;
                List<t_liste_reduite_nematode> ListeReduiteNemaPleine = c.t_liste_reduite_nematode.ToList();
                List<t_liste_reduite_nematode> ListeReduiteNema  = new List<t_liste_reduite_nematode>();
                foreach (var i in ListeReduiteNemaPleine)
                {
                    if (i.ilistereduite == id)
                    {
                        ListeReduiteNema.Add(i);
                    }
                }
                Ajouter_liste_nematode mUpListNema = new Ajouter_liste_nematode(ListeReduiteNema, ListeReduite.cnom);
                mUpListNema.Show();
            }            
        }

        private void bAddList_Click(object sender, EventArgs e)
        {
            Ajouter_liste_nematode mCreateListNema = new Ajouter_liste_nematode();
            mCreateListNema.Show();
        }

        private void bValiderListe_Click(object sender, EventArgs e)
        {
            if (bValiderListe.Text == "Valider la liste" )
            {
                cbListReduiteNema.Enabled = false;
                bValiderListe.Text = "Dévalider la liste";
            }
            else
            {
                cbListReduiteNema.Enabled = true;
                bValiderListe.Text = "Valider la liste";
            }
        }

        // Lancement de la reconnaissance vocale
        private void bPlay_Click(object sender, EventArgs e)
        {
            Sim_RecoVocale srv = new Sim_RecoVocale();

            /// debut de la reconnaissance 
            Reconnaissance reco1 = new Reconnaissance();
            //SystemSounds.Beep.Play();
            monMicro mic = new monMicro();

            // LG 20181130 old            mic.lanceEnregistre(GetFolder(monMicro.NomFichier));
            String leFichier = GetNextWavFile();
            mic.lanceEnregistre(leFichier);

            Thread.Sleep(5000);
            CultureInfo current = CultureInfo.CurrentCulture;

            using (SpeechRecognitionEngine recognizer = new SpeechRecognitionEngine())
            {
                var tab = srv.GenerateTabForDictionnary(nemaList);

                // Create a grammar, construct a Grammar object, and load it to the recognizer.
                Choices nematodes = new Choices(tab);
                GrammarBuilder test = new GrammarBuilder(nematodes);
                Grammar testing = new Grammar(test);
                recognizer.LoadGrammar(testing);
                
                // Configure the input to the recognizer.
                recognizer.SetInputToWaveFile(monMicro.NomFichier);

                // Attach event handlers.
                recognizer.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(Reconnaissance.SpeechRecognizedHandler);
                recognizer.RecognizeCompleted += new EventHandler<RecognizeCompletedEventArgs>(Reconnaissance.RecognizeCompletedHandler);
                Reconnaissance.completed = false;
                recognizer.RecognizeAsync(RecognizeMode.Multiple);

                //while (!Reconnaissance.completed)
                //{
                //    Thread.Sleep(333);
                //}
            }
            using (db_entities c = new db_entities())
            {
                string nem = Reconnaissance.nematode;
                if (nem != null)
                {
                    t_nematodes n = c.t_nematodes.FirstOrDefault(ne => ne.cnom_court == Reconnaissance.nematode);


                    //Random rnd = new Random();
                    //int i = rnd.Next(0, nemaList.Count);
                    //int id = nemaList[i].id;

                    //t_nematodes n = c.t_nematodes.FirstOrDefault(ne => ne.id == id);

                    mesDictees.Add(new CSVDictee()
                    {
                        NomDictee = thisEchantillon.cnom + "." + DateTime.Now.ToString("dd-MM-yy_HH.mm.ss") + ".wav",
                        NomNematode = n.cnom_court
                    });
                    srv.GenerateDicteeRow(thisEchantillon, n);
                    srv.GenerateDicteeCsv(GetFolder("CSVDictee.csv"), mesDictees);
                    mesDictees.Clear();
                }
                else
                {
                    MessageBox.Show("Nématode non reconnue : Simulation de la detection (ajout d'une valeure aléatoire car détéction ratée)");
                    Random rnd = new Random();
                    int i = rnd.Next(0, nemaList.Count);
                    int id = nemaList[i].id;

                    t_nematodes n = c.t_nematodes.FirstOrDefault(ne => ne.id == id);
                    //t_nematodes n = c.t_nematodes.FirstOrDefault(ne => ne.cnom_court == Reconnaissance.nematode);

                    mesDictees.Add(new CSVDictee()
                    {
                        NomDictee = thisEchantillon.cnom + "." + DateTime.Now.ToString("dd-MM-yy_HH.mm.ss") + ".wav",
                        NomNematode = n.cnom_court
                    });
                    srv.GenerateDicteeRow(thisEchantillon, n);
                    srv.GenerateDicteeCsv(GetFolder("CSVDictee.csv"), mesDictees);
                    mesDictees.Clear();
                }
               
            }
            chargerNematode();
        }

        // Ne sert à rien pour l'instant
        private void timer1_Tick(object sender, EventArgs e)
        {
            using (db_entities c = new db_entities())
            {
                
                #region Ajoute une nématode
                Random rnd = new Random();
                int i = rnd.Next(0, nemaList.Count);
                int id = nemaList[i].id;
                t_nematodes n = c.t_nematodes.FirstOrDefault(ne => ne.id == id);
                Sim_RecoVocale srv = new Sim_RecoVocale();
                srv.GenerateDicteeRow(thisEchantillon, n); 
                #endregion

                chargerNematode();
            }
        }

        // Vide puis reremplis le DataGridView avec les nématodes de l'échantillon puis leur quantitée
        public void chargerNematode()
        {
            dgvNematodes.Rows.Clear();
            Dictionary<string, int> NemaQuantitee = new Dictionary<string, int>();
            using (db_entities context = new db_entities())
            {
                nomEchantillon = cbEchantillon.SelectedItem.ToString();
                thisEchantillon = context.t_echantillons.FirstOrDefault(c => c.cnom == nomEchantillon);
                t_liste_reduite ListeReduite = context.t_liste_reduite.FirstOrDefault(lr => lr.id == thisEchantillon.tlistereduite_id);
                int id = ListeReduite.id; // id correspondante à la liste réduite
                List<t_liste_reduite_nematode> ListeReduiteNemaPleine = context.t_liste_reduite_nematode.ToList();
                List<t_liste_reduite_nematode> ListeReduiteNema = new List<t_liste_reduite_nematode>();
                List<t_dictee> listdictee = context.t_dictee.ToList(); // liste des dictées
                List<int> quantite = new List<int>(); // retourne la quantitée de nematodes.

                foreach (var i in ListeReduiteNemaPleine)
                {
                    if (i.ilistereduite == id)// ajoute toutes les id nématodes ayant pour id cette liste réduite
                    {
                        ListeReduiteNema.Add(i);
                    }
                }
                foreach (var l in ListeReduiteNema)// pour toutes les nématodes de la liste réduite
                {
                    int quantité = 0;
                    foreach (var q in listdictee) //pour chaques dictée de la liste dictée
                    {
                        if (q.iechantillons == thisEchantillon.id && l.inematodes == q.inematodes) // Si bon echantillon et bonne nematode
                        {
                            quantité = quantité + 1; // pour chaque fichier WAV aux bonnes caractéristiques, ajouter 1
                        }
                    }
                    nemaList.Add(context.t_nematodes.FirstOrDefault(n => n.id == l.inematodes));
                    var nema = context.t_nematodes.FirstOrDefault(n => n.id == l.inematodes);
                    NemaQuantitee.Add(nema.cnom_court, quantité); // Dictionnary --> Nom,  Quantitée
                }        
                foreach (var nema in NemaQuantitee)
                {
                    dgvNematodes.Rows.Add(nema.Key, nema.Value); //Ajouter à la DataGridView le dictionnary avec Nom : nema.Key, Quantitée : nema.Value    
                }
                ReSize();
            }
        }

        // Change la taille de la DataGridView en fonction des colonnes
        public void ReSize()
        {
            int Height = 0;
            foreach (DataGridViewRow row in dgvNematodes.Rows)
            {
                if (Height <= 200)
                {
                    Height += row.Height;
                }
            }
            Height += dgvNematodes.ColumnHeadersHeight;

            int Width = 0 - 40;
            foreach (DataGridViewColumn col in dgvNematodes.Columns)
            {
                Width += col.Width;
            }
            Width += dgvNematodes.RowHeadersWidth;
            if (Height < 200)
            {
                dgvNematodes.ClientSize = new Size(Width + 2, Height + 2);
            }
            else dgvNematodes.ClientSize = new Size(Width + 20, Height + 2);
            gbEchantillon.AutoSize = true;
            AutoSize = true;
        }

        // Créer le dossier
        public string GetFolder(string filename)
        {                                                                                                                                                                                                                                                                               
            string folderNameClient = thisClient.cnom_entreprise;
            string folderNameDossier = folderNameClient + @"\" + thisDossier.cnom;
            string folderNameEchantillon = folderNameDossier + @"\" + thisEchantillon.cnom;
            Directory.CreateDirectory(folderNameEchantillon); // creation d'un dossier à partir de son chemin
            return folderNameEchantillon + @"\" + filename;
        }

        // LG 20181130
        public string GetNextWavFile()
        {                                                                                                                                                                                                                                                                               
            return GetFolder(DateTime.Now.ToString("dd-MM-yy_HH.mm.ss") + ".wav") ;
        }

        private void NemaNet_Load(object sender, EventArgs e)
        {
            // TODO: cette ligne de code charge les données dans la table 'elisolDataSet1.tClients'. Vous pouvez la déplacer ou la supprimer selon les besoins.
        //    this.tClientsTableAdapter1.Fill(this.elisolDataSet1.tClients);

        }

        private void tClientsBindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void cbClient_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
