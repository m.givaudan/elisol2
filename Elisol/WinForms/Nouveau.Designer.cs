﻿namespace Elisol2
{
    partial class Nouveau
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bValider = new System.Windows.Forms.Button();
            this.bAnnuler = new System.Windows.Forms.Button();
            this.gbClient = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbNum_Siren = new System.Windows.Forms.TextBox();
            this.tbNum_Commande = new System.Windows.Forms.TextBox();
            this.tbCode_Postal = new System.Windows.Forms.TextBox();
            this.tbNum_Tel = new System.Windows.Forms.TextBox();
            this.tbNom_Entreprise = new System.Windows.Forms.TextBox();
            this.tbAdresse = new System.Windows.Forms.TextBox();
            this.gbEchantillon = new System.Windows.Forms.GroupBox();
            this.cbListReduiteNema = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.bAddList = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.tbNom_Echantillon = new System.Windows.Forms.TextBox();
            this.gbDossier = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbNom_Dossier = new System.Windows.Forms.TextBox();
            this.gbClient.SuspendLayout();
            this.gbEchantillon.SuspendLayout();
            this.gbDossier.SuspendLayout();
            this.SuspendLayout();
            // 
            // bValider
            // 
            this.bValider.Location = new System.Drawing.Point(127, 71);
            this.bValider.Name = "bValider";
            this.bValider.Size = new System.Drawing.Size(102, 45);
            this.bValider.TabIndex = 6;
            this.bValider.Text = "Valider";
            this.bValider.UseVisualStyleBackColor = true;
            this.bValider.Click += new System.EventHandler(this.bValider_Click);
            // 
            // bAnnuler
            // 
            this.bAnnuler.Location = new System.Drawing.Point(19, 71);
            this.bAnnuler.Name = "bAnnuler";
            this.bAnnuler.Size = new System.Drawing.Size(102, 45);
            this.bAnnuler.TabIndex = 5;
            this.bAnnuler.Text = "Annuler";
            this.bAnnuler.UseVisualStyleBackColor = true;
            this.bAnnuler.Click += new System.EventHandler(this.bAnnuler_Click);
            // 
            // gbClient
            // 
            this.gbClient.Controls.Add(this.label7);
            this.gbClient.Controls.Add(this.label6);
            this.gbClient.Controls.Add(this.label5);
            this.gbClient.Controls.Add(this.label4);
            this.gbClient.Controls.Add(this.label3);
            this.gbClient.Controls.Add(this.label2);
            this.gbClient.Controls.Add(this.tbNum_Siren);
            this.gbClient.Controls.Add(this.tbNum_Commande);
            this.gbClient.Controls.Add(this.tbCode_Postal);
            this.gbClient.Controls.Add(this.tbNum_Tel);
            this.gbClient.Controls.Add(this.tbNom_Entreprise);
            this.gbClient.Controls.Add(this.tbAdresse);
            this.gbClient.Location = new System.Drawing.Point(241, 161);
            this.gbClient.Name = "gbClient";
            this.gbClient.Size = new System.Drawing.Size(223, 181);
            this.gbClient.TabIndex = 10;
            this.gbClient.TabStop = false;
            this.gbClient.Text = "Client";
            this.gbClient.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 149);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "N°Siren :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 123);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "N°Commande : ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 97);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Code Postal : ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Tel : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Nom de l\'entreprise : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Adresse : ";
            // 
            // tbNum_Siren
            // 
            this.tbNum_Siren.Location = new System.Drawing.Point(117, 149);
            this.tbNum_Siren.Name = "tbNum_Siren";
            this.tbNum_Siren.Size = new System.Drawing.Size(100, 20);
            this.tbNum_Siren.TabIndex = 6;
            // 
            // tbNum_Commande
            // 
            this.tbNum_Commande.Location = new System.Drawing.Point(117, 123);
            this.tbNum_Commande.Name = "tbNum_Commande";
            this.tbNum_Commande.Size = new System.Drawing.Size(100, 20);
            this.tbNum_Commande.TabIndex = 5;
            // 
            // tbCode_Postal
            // 
            this.tbCode_Postal.Location = new System.Drawing.Point(117, 97);
            this.tbCode_Postal.Name = "tbCode_Postal";
            this.tbCode_Postal.Size = new System.Drawing.Size(100, 20);
            this.tbCode_Postal.TabIndex = 4;
            // 
            // tbNum_Tel
            // 
            this.tbNum_Tel.Location = new System.Drawing.Point(117, 71);
            this.tbNum_Tel.Name = "tbNum_Tel";
            this.tbNum_Tel.Size = new System.Drawing.Size(100, 20);
            this.tbNum_Tel.TabIndex = 3;
            // 
            // tbNom_Entreprise
            // 
            this.tbNom_Entreprise.Location = new System.Drawing.Point(117, 45);
            this.tbNom_Entreprise.Name = "tbNom_Entreprise";
            this.tbNom_Entreprise.Size = new System.Drawing.Size(100, 20);
            this.tbNom_Entreprise.TabIndex = 2;
            // 
            // tbAdresse
            // 
            this.tbAdresse.Location = new System.Drawing.Point(117, 19);
            this.tbAdresse.Name = "tbAdresse";
            this.tbAdresse.Size = new System.Drawing.Size(100, 20);
            this.tbAdresse.TabIndex = 1;
            // 
            // gbEchantillon
            // 
            this.gbEchantillon.Controls.Add(this.cbListReduiteNema);
            this.gbEchantillon.Controls.Add(this.label1);
            this.gbEchantillon.Controls.Add(this.bAddList);
            this.gbEchantillon.Controls.Add(this.label8);
            this.gbEchantillon.Controls.Add(this.tbNom_Echantillon);
            this.gbEchantillon.Location = new System.Drawing.Point(241, 12);
            this.gbEchantillon.Name = "gbEchantillon";
            this.gbEchantillon.Size = new System.Drawing.Size(223, 129);
            this.gbEchantillon.TabIndex = 9;
            this.gbEchantillon.TabStop = false;
            this.gbEchantillon.Text = "Echantillon";
            this.gbEchantillon.Visible = false;
            // 
            // cbListReduiteNema
            // 
            this.cbListReduiteNema.DisplayMember = "cNom";
            this.cbListReduiteNema.FormattingEnabled = true;
            this.cbListReduiteNema.Location = new System.Drawing.Point(9, 59);
            this.cbListReduiteNema.Name = "cbListReduiteNema";
            this.cbListReduiteNema.Size = new System.Drawing.Size(208, 21);
            this.cbListReduiteNema.TabIndex = 29;
            this.cbListReduiteNema.ValueMember = "cNom";
            this.cbListReduiteNema.SelectionChangeCommitted += new System.EventHandler(this.cbListReduiteNema_SelectionChangeCommitted);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(180, 13);
            this.label1.TabIndex = 28;
            this.label1.Text = "Selectionnez une liste de nématodes";
            // 
            // bAddList
            // 
            this.bAddList.Location = new System.Drawing.Point(59, 86);
            this.bAddList.Name = "bAddList";
            this.bAddList.Size = new System.Drawing.Size(107, 34);
            this.bAddList.TabIndex = 27;
            this.bAddList.Text = "Créer une liste";
            this.bAddList.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 19);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Nom : ";
            // 
            // tbNom_Echantillon
            // 
            this.tbNom_Echantillon.Location = new System.Drawing.Point(117, 19);
            this.tbNom_Echantillon.Name = "tbNom_Echantillon";
            this.tbNom_Echantillon.Size = new System.Drawing.Size(100, 20);
            this.tbNom_Echantillon.TabIndex = 2;
            // 
            // gbDossier
            // 
            this.gbDossier.Controls.Add(this.label9);
            this.gbDossier.Controls.Add(this.tbNom_Dossier);
            this.gbDossier.Location = new System.Drawing.Point(12, 12);
            this.gbDossier.Name = "gbDossier";
            this.gbDossier.Size = new System.Drawing.Size(223, 53);
            this.gbDossier.TabIndex = 8;
            this.gbDossier.TabStop = false;
            this.gbDossier.Text = "Dossier";
            this.gbDossier.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 19);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Nom : ";
            // 
            // tbNom_Dossier
            // 
            this.tbNom_Dossier.Location = new System.Drawing.Point(117, 19);
            this.tbNom_Dossier.Name = "tbNom_Dossier";
            this.tbNom_Dossier.Size = new System.Drawing.Size(100, 20);
            this.tbNom_Dossier.TabIndex = 2;
            // 
            // Nouveau
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 336);
            this.Controls.Add(this.gbClient);
            this.Controls.Add(this.gbEchantillon);
            this.Controls.Add(this.gbDossier);
            this.Controls.Add(this.bValider);
            this.Controls.Add(this.bAnnuler);
            this.Name = "Nouveau";
            this.Text = "Nouveau";
            this.gbClient.ResumeLayout(false);
            this.gbClient.PerformLayout();
            this.gbEchantillon.ResumeLayout(false);
            this.gbEchantillon.PerformLayout();
            this.gbDossier.ResumeLayout(false);
            this.gbDossier.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button bValider;
        private System.Windows.Forms.Button bAnnuler;
        private System.Windows.Forms.GroupBox gbClient;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbNum_Siren;
        private System.Windows.Forms.TextBox tbNum_Commande;
        private System.Windows.Forms.TextBox tbCode_Postal;
        private System.Windows.Forms.TextBox tbNum_Tel;
        private System.Windows.Forms.TextBox tbNom_Entreprise;
        private System.Windows.Forms.TextBox tbAdresse;
        private System.Windows.Forms.GroupBox gbEchantillon;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbNom_Echantillon;
        private System.Windows.Forms.GroupBox gbDossier;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbNom_Dossier;
        private System.Windows.Forms.ComboBox cbListReduiteNema;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bAddList;
    }
}