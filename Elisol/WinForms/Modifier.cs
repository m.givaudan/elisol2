﻿using System;
using System.Collections.Generic;
using Elisol.Entity_Data_Models.ElisolModels;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;

namespace Elisol
{
    public partial class Modifier : Form
    {
        t_clients client;
        t_dossiers dossier;
        t_echantillons echantillon;

        string NomListNema;
        t_liste_reduite LR;

        t_clients PrevClient;
        t_dossiers PrevDossier;
        t_echantillons PrevEchantillon;

        string type;
        bool create = false;

        public Modifier(string s)
        {
            InitializeComponent();
            type = s;
            create = true;

            #region IHM d'un client
            gbDossier.Hide();
            gbEchantillon.Hide();

            gbClient.Show();
            gbClient.Location = new Point(15, 15);

            bValider.Location = new Point(gbClient.Location.X, tbNum_Siren.Bottom + 40);
            bAnnuler.Location = new Point(bValider.Location.X + 120, tbNum_Siren.Bottom + 40);

            Size = new Size(gbClient.Right + 30, bValider.Bottom + 50);
            #endregion   
        }

        public Modifier(string s, t_clients c)
        {
            InitializeComponent();
            type = s;
            client = c;
            create = true;

            #region IHM d'un dossier
            gbClient.Hide();
            gbEchantillon.Hide();

            gbDossier.Show();
            gbDossier.Location = new Point(15, 15);

            bValider.Location = new Point(gbDossier.Location.X, tbNom_Dossier.Bottom + 40);
            bAnnuler.Location = new Point(bValider.Location.X + 120, tbNom_Dossier.Bottom + 40);

            Size = new Size(gbDossier.Right + 30, bValider.Bottom + 50);
            #endregion
        }

        public Modifier(string s, t_dossiers d)
        {
            InitializeComponent();
            type = s;
            dossier = d;
            create = true;

            #region IHM d'un echantillon
            using (var context = new db_entities())
            {
                var ListeReduite = context.t_liste_reduite.ToList();
                cbListReduiteNema.DataSource = ListeReduite;
                cbListReduiteNema.SelectedIndex = -1;
                cbListReduiteNema.Text = "Selectionnez une liste réduite";
            }

            gbClient.Hide();
            gbDossier.Hide();

            gbEchantillon.Show();
            gbEchantillon.Location = new Point(15, 15);

            bValider.Location = new Point(gbEchantillon.Location.X, bAddList.Bottom + 40);
            bAnnuler.Location = new Point(bValider.Location.X + 120, bAddList.Bottom + 40);

            Size = new Size(gbEchantillon.Right + 30, bValider.Bottom + 50);
            #endregion
        }

        public Modifier(t_clients c)
        {
            InitializeComponent();
            gbDossier.Hide();
            gbEchantillon.Hide();
            type = "t_clients";
            gbClient.Show();
            gbClient.Location = new Point(15,15);
            client = c;

            #region Mise en place de la WinForm

            Text = c.cnom_entreprise;

            //tbID_Client.Text = c.iID_Clients.ToString();
            tbAdresse.Text = c.cadresse;
            tbNom_Entreprise.Text = c.cnom_entreprise;
            tbNum_Tel.Text = c.cnum_tel;
            tbNom_Ville.Text = c.cville;
            tbCode_Postal.Text = c.ccode_postal;
            tbNum_Siren.Text = c.cnum_siren;
            
            bValider.Location = new Point(gbClient.Location.X, tbNum_Siren.Bottom + 40);
            bAnnuler.Location = new Point(bValider.Location.X + 120, tbNum_Siren.Bottom + 40);

            Size = new Size(gbClient.Right + 30, bValider.Bottom + 50);
            #endregion
        }

        public Modifier(t_dossiers d)
        {
            InitializeComponent();
            gbClient.Hide();
            gbEchantillon.Hide();
            type = "t_dossiers";
            gbDossier.Show();
            gbDossier.Location = new Point(15, 15);
            dossier = d;

            #region Mise en place de la WinForm

            Text = d.cnom;

            tbNom_Dossier.Text = dossier.cnom;

            bValider.Location = new Point(gbDossier.Location.X, tbNom_Dossier.Bottom + 40);
            bAnnuler.Location = new Point(bValider.Location.X + 120, tbNom_Dossier.Bottom + 40);

            Size = new Size(gbDossier.Right + 30, bValider.Bottom + 50);

            #endregion
        }

        public Modifier(t_echantillons e)
        {
            InitializeComponent();

            gbClient.Hide();
            gbDossier.Hide();
            type = "t_echantillons";
            gbEchantillon.Show();
            gbEchantillon.Location = new Point(15, 15);
            echantillon = e;


            // LG 20181130 début
            #region RemplitListe_ListeNématodes
            using (db_entities context = new db_entities())                     // Utilise un objet de connection à la BDD
            {
                // Parcourir toutes les lignes de la table Liste_Réduite
                // Pour ajouter pour chacune un item à la liste
                // Et trouver la liste actuellement sélectionnée pour cet échantillon
                int index = 0;
                int selectedIndex = -1;
                var ListeReduite = context.t_liste_reduite.ToList();
                foreach (var item in ListeReduite)
                {
                    cbListReduiteNema.Items.Add(item.cnom);
                    if (item.id == echantillon.tlistereduite_id) {
                        // On a trouvé l'item correspondant à cet échantillon
                        selectedIndex = index;
                    }
                    index++;
                }
                cbListReduiteNema.SelectedIndex = selectedIndex;
                //cbListReduiteNema.Text = "Selectionnez une liste réduite";
            }
            #endregion
            // LG 201830 Fin

            #region Mise en place de la WinForm

            Text = echantillon.cnom;

            tbNom_Echantillon.Text = echantillon.cnom;

            // LG 20181130 début
            // bValider.Location = new Point(gbEchantillon.Location.X, tbNom_Echantillon.Bottom + 40);
            // bAnnuler.Location = new Point(bValider.Location.X + 120, tbNom_Echantillon.Bottom + 40);
            bValider.Location = new Point(gbEchantillon.Location.X, gbEchantillon.Bottom + 2);
            bAnnuler.Location = new Point(bValider.Location.X + 120, gbEchantillon.Bottom + 2);
            // LG 20181130 fin

            Size = new Size(gbEchantillon.Right + 30, bValider.Bottom + 50);
            #endregion
        }

        private void cbListReduiteNema_SelectionChangeCommitted(object sender, EventArgs e)
        {
            // LG 201830 Début
            if (cbListReduiteNema.SelectedItem == null)
            {
                return;
            }
            // LG 201830 Fin

            NomListNema = cbListReduiteNema.SelectedItem.ToString();        // LG 20181130 SelectedValue Changé en SelectedItem
            if (NomListNema != "Selectionnez une liste réduite")
            {
                using (var c = new db_entities())
                {
                    LR = c.t_liste_reduite.FirstOrDefault(lr => lr.cnom == NomListNema);
                }
                bValider.Enabled = true;
            }
        }

        private void bAnnuler_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void bValider_Click(object sender, EventArgs e)
        {
            // faire l'update
            using (var c = new db_entities())
            {
                string textinfo = "Etes-vous sûr de vouloir valider ces informations ?";
                string info = "Voulez-vous vraiment créer ";

                if (type == "t_clients")
                {
                    if (create == true)
                    {
                        #region Création d'un client
                        c.t_clients.Add(new t_clients()
                        {
                            cadresse = tbAdresse.Text,
                            cnom_entreprise = tbNom_Entreprise.Text,
                            cnum_tel = tbNum_Tel.Text,
                            cville = tbNom_Ville.Text,
                            ccode_postal = tbCode_Postal.Text,
                            cnum_siren = tbNum_Siren.Text
                        });
                        textinfo = info + tbNom_Entreprise.Text;
                        #endregion
                    }
                    else
                    {
                        #region Modification d'un client
                        string PrevAdd = client.cadresse;
                        client.cadresse = tbAdresse.Text;
                        string PrevNomEntreprise = client.cnom_entreprise;
                        client.cnom_entreprise = tbNom_Entreprise.Text;
                        string PrevNum_Tel = client.cnum_tel;
                        client.cnum_tel = tbNum_Tel.Text;
                        string PrevNom_Ville = client.cville;
                        client.cville = tbNom_Ville.Text;
                        string PrevCode_Postal = client.ccode_postal;
                        client.ccode_postal = tbCode_Postal.Text;
                        string PrevNum_Siren = client.cnum_siren;
                        client.cnum_siren = tbNum_Siren.Text;

                        t_clients PrevClient = new t_clients()
                        {
                            id = client.id,
                            cadresse = PrevAdd,
                            cnom_entreprise = PrevNomEntreprise,
                            cnum_tel = PrevNum_Tel,
                            cville = PrevNom_Ville,
                            ccode_postal = PrevCode_Postal,
                            cnum_siren = PrevNum_Siren
                        };

                        c.t_clients.Attach(client);
                        c.Entry(client).State = EntityState.Modified;

                        if (client.cadresse != PrevAdd)
                        {
                            textinfo = textinfo + "\n" + PrevAdd + " ==> " + client.cadresse;
                        }
                        if (client.cnom_entreprise != PrevNomEntreprise)
                        {
                            textinfo = textinfo + "\n" + PrevNomEntreprise + " ==> " + client.cnom_entreprise;
                        }
                        if (client.cnum_tel != PrevNum_Tel)
                        {
                            textinfo = textinfo + "\n" + PrevNum_Tel + " ==> " + client.cnum_tel;
                        }
                        if (client.cville.ToString() != PrevNom_Ville.ToString())
                        {
                            textinfo = textinfo + "\n" + PrevNom_Ville.ToString() + " ==> " + client.cville.ToString();
                        }
                        if (client.ccode_postal.ToString() != PrevCode_Postal.ToString())
                        {
                            textinfo = textinfo + "\n" + PrevCode_Postal.ToString() + " ==> " + client.ccode_postal.ToString();
                        }
                        if (client.cnum_siren.ToString() != PrevNum_Siren.ToString())
                        {
                            textinfo = textinfo + "\n" + PrevNum_Siren.ToString() + " ==> " + client.cnum_siren.ToString();
                        }
                        #endregion
                    }
                }
                if (type == "t_dossiers")
                {
                    if (create == true)
                    {
                        #region Création d'un dossier
                        c.t_dossiers.Add(new t_dossiers()
                        {
                            cnom = tbNom_Dossier.Text,
                            client_id = client.id
                        });
                        textinfo = info + tbNom_Dossier.Text;
                        #endregion
                    }
                    else
                    {
                        #region Modification d'un dossier
                        string PrevNom = dossier.cnom;
                        dossier.cnom = tbNom_Dossier.Text;

                        t_dossiers PrevDossier = new t_dossiers()
                        {
                            id = dossier.id,
                            cnom = PrevNom,
                            client_id = dossier.client_id
                        };

                        c.t_dossiers.Attach(dossier);
                        c.Entry(dossier).State = EntityState.Modified;

                        if (dossier.cnom != PrevNom)
                        {
                            textinfo = textinfo + "\n" + PrevNom + " ==> " + dossier.cnom;
                        }
                        #endregion
                    }
                }
                if (type == "t_echantillons")
                {
                    if (create == true)
                    {
                        #region Création d'un echantillon
                        
                        c.t_echantillons.Add(new t_echantillons()
                        {
                            cnom = tbNom_Echantillon.Text,
                            tdossier_id = dossier.id,
                            tlistereduite_id = LR.id
                        });
                        textinfo = info + tbNom_Echantillon.Text;
                        #endregion
                    }
                    else
                    {
                        #region Modification d'un echantillon
                        string PrevNomEchantillon = echantillon.cnom;
                        echantillon.cnom = tbNom_Echantillon.Text;
                        // LG 20181130 début
                        string nomListeReduite = cbListReduiteNema.SelectedItem.ToString() ;
                        t_liste_reduite ListeRéduite = c.t_liste_reduite.FirstOrDefault(LR => LR.cnom == nomListeReduite);
                        int idListeReduite = ListeRéduite.id ;
                        echantillon.tlistereduite_id = idListeReduite ;
                        echantillon.t_liste_reduite = ListeRéduite;
                        // LG 20181130 fin

                        t_echantillons PrevEchantillon = new t_echantillons()
                        {
                            id = echantillon.id,
                            cnom = PrevNomEchantillon,
                            tdossier_id = echantillon.tdossier_id,
                            tlistereduite_id = echantillon.tlistereduite_id
                        };

                        c.t_echantillons.Attach(echantillon);
                        c.Entry(echantillon).State = EntityState.Modified;

                        if (echantillon.cnom != PrevNomEchantillon)
                        {
                            textinfo = textinfo + "\n" + PrevNomEchantillon + " ==> " + echantillon.cnom;
                        }
                        #endregion
                    }
                }

                if (MessageBox.Show(this, textinfo, "!! Warning !!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes && textinfo != "Etes-vous sûr de vouloir valider ces informations ?")
                {
                    c.SaveChanges();
                    Close();
                }
                client = PrevClient;
                dossier = PrevDossier;
                echantillon = PrevEchantillon;
            }    
        }

        private void bAddList_Click(object sender, EventArgs e)
        {
            Ajouter_liste_nematode newListNema = new Ajouter_liste_nematode();
            newListNema.Show();
        }
    }
}
