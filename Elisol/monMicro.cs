﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.IO;
using System.Threading;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework;
using System.Media;

namespace Elisol
{
    class monMicro
    {
        Microphone mic;                     // Object representing the physical microphone on the device 
        byte[] buffer;                      // Dynamic buffer to retrieve audio data from the microphone 
        private MemoryStream stream;       // Stores the audio data for later playback 
        public Boolean lEnregistre = true;
        public static string NomFichier = (DateTime.Now.ToString("dd-MM-yy_HH.mm.ss") + ".wav");

        //enregistrement du son
        public void lanceEnregistre(string NewNomFichier, int piDuréeSecondes = 3000)
        {
            //NewNomFichier = NomFichier;
            NomFichier = NewNomFichier;
            // NE SEMBLE PLUS UTILE CAR PLACE DANS TIMER
            // http://stackoverflow.com/questions/6807963/media-player-in-windows-phone-7
            // https://msdn.microsoft.com/library/ff842408.aspx
            // FrameworkDispatcher.Update();

            mic = Microphone.Default;
            stream = new MemoryStream();
            stream.SetLength(0);            // Set the stream back to zero in case there is already something in it 

            // Get audio data in 1/2 second chunks
            mic.BufferDuration = TimeSpan.FromMilliseconds(1000);
            buffer = new byte[mic.GetSampleSizeInBytes(mic.BufferDuration)];

            // Timer pour FrameworkDispatcher.Update(), qui semble nécesaire périodiquement (ici on le fait ttes les 200ms)
            System.Threading.Timer loTimerPourFrameworkDispatcherUpdate = new System.Threading.Timer(ticPourFrameworkDispatcherUpdate, null, 0, 750);

            // Timer pour fin de l'enregistrement (ne se déclanche qu'une fois)
            System.Threading.Timer loTimerPourTermineEnregistre = new System.Threading.Timer(termineEnregistre, null, piDuréeSecondes, 0);

            mic.Start();
            mic.BufferReady += new EventHandler<EventArgs>(microphone_BufferReady);     // Event handler for getting audio data when the buffer is full 

        }

        /// <summary> 
        /// The Microphone.BufferReady event handler. 
        /// Gets the audio data from the microphone and stores it in a buffer, 
        /// then writes that buffer to a stream for later playback. 
        /// Any action in this event handler should be quick! 
        /// </summary> 
        /// <param name="sender"></param> 
        /// <param name="e"></param> 
        void microphone_BufferReady(object sender, EventArgs e)
        {
            // Indiquer qu'on a recu
            //Console.WriteLine("Recu : \n");

            // Retrieve audio data 
            mic.GetData(buffer);

            // Store the audio data in the stream 
            stream.Write(buffer, 0, buffer.Length);

        }

        void ticPourFrameworkDispatcherUpdate(object o)
        {
            // Selon https://social.msdn.microsoft.com/Forums/windowsapps/en-US/6db6d4d8-d7b1-44a4-b675-495f2447ebd8/microphone-bufferready-event-handler-doesnt-get-in-windows-phone-8-app?forum=wpdevelop
            // Il faut faire appel à FrameworkDispatcher.Update dans un timer pour que le bufferReady soit appellé
            // voir aussi http://stackoverflow.com/questions/6807963/media-player-in-windows-phone-7
            // voir aussi https://msdn.microsoft.com/library/ff842408.aspx
            //            Console.WriteLine("ticPourFrameworkDispatcherUpdate");
            try { FrameworkDispatcher.Update(); } catch { }
        }

        void termineEnregistre(object o)
        {

            // Indiquer qu'on a recu
            Console.WriteLine("termineEnregistre\n");
            
            // terminer l'enregistrement
            mic.Stop();
            SystemSounds.Beep.Play();
            lEnregistre = false;

            // Générer le fichier
            saveWav();

        }

        //creation et enregistrement fichier WAV
        string saveWav()
        {
            //string NomFichier = (DateTime.Now.ToString("dd-MM-yy_HH.mm.ss") + ".wav");
            const Int32 WAV_FREQUENCE_16kHz = 16000;
            const Int32 WAV_FREQUENCE_44kHz = 44100;
            const Int32 WAV_BITPERSAMPLE_16 = 16;
            const Int32 WAV_HEADERSIZE = 44;        // Taille de l'en-tête WAV, selon https://fr.wikipedia.org/wiki/WAVEform_audio_format


            // selon http://stackoverflow.com/questions/14659684/creating-a-wav-file-in-c-sharp
            uint numsamples = 44100;
            ushort numchannels = 1;
            ushort samplelength = (ushort)buffer.Length; // in bytes
                                                         //            uint samplerate = 22050;
            uint liFréquence = WAV_FREQUENCE_44kHz;     // Il semble que la fréquence d'échantillonnage soit de 44khz ici (alors qu'elle est de 16kHz avec Sphynx)
            Int32 liBitPerSample = WAV_BITPERSAMPLE_16;
            Int32 liTailleData = buffer.Length * 4;     // POURQUOI FAUT-IL MULTIPLIER PAR 4 ??? (valeur obtenue par comparaison avec le même code en C)
            Int32 liTailleFichierMoins8 = liTailleData + WAV_HEADERSIZE - 8;
            //Console.Write(path); test format horaire

            FileStream f = new FileStream(NomFichier, FileMode.Create);
            BinaryWriter wr = new BinaryWriter(f);
            // selon https://fr.wikipedia.org/wiki/WAVEform_audio_format

            // [Bloc de déclaration d'un fichier au format WAVE]
            wr.Write(Encoding.ASCII.GetBytes("RIFF"));      //FileTypeBlocID  (4 octets) : Constante «RIFF»  (0x52,0x49,0x46,0x46)
                                                            //            wr.Write(36 + numsamples * numchannels * samplelength);
            wr.Write(liTailleFichierMoins8);                // FileSize        (4 octets) : Taille du fichier moins 8 octets
            wr.Write(Encoding.ASCII.GetBytes("WAVE"));      // FileFormatID    (4 octets) : Format = «WAVE»  (0x57,0x41,0x56,0x45)

            // [Bloc décrivant le format audio]
            wr.Write(Encoding.ASCII.GetBytes("fmt "));      // FormatBlocID    (4 octets) : Identifiant «fmt »  (0x66,0x6D, 0x74,0x20)
            wr.Write((uint)16);                             // BlocSize        (4 octets) : Nombre d'octets du bloc - 16  (0x10)
            wr.Write((ushort)1);                            // AudioFormat     (2 octets) : Format du stockage dans le fichier (1: PCM, ...)
            wr.Write(numchannels);                          // NbrCanaux       (2 octets) : Nombre de canaux (de 1 à 6)
                                                            //            wr.Write(samplerate);
                                                            //            wr.Write(samplerate * samplelength * numchannels);
                                                            //            wr.Write(samplelength * numchannels);
                                                            //            wr.Write((ushort)(8 * samplelength));
            wr.Write((uint)liFréquence);                              // Frequence       (4 octets) : Fréquence d'échantillonnage (en hertz) [Valeurs standardisées : 11025, 22050, 44100 et éventuellement 48000 et 96000]
            wr.Write((uint)(liFréquence * liBitPerSample / 8)); // BytePerSec      (4 octets) : Nombre d'octets à lire par seconde (c.-à-d., Frequence * BytePerBloc).
            wr.Write((ushort)(liBitPerSample / 8));             // BytePerBloc     (2 octets) : Nombre d'octets par bloc d'échantillonnage (c.-à-d., tous canaux confondus : NbrCanaux * BitsPerSample/8).
            wr.Write((ushort)(liBitPerSample));                 // BitsPerSample   (2 octets) : Nombre de bits utilisés pour le codage de chaque échantillon (8, 16, 24)
                                                                //            wr.Write(Encoding.ASCII.GetBytes("TEST"));
            wr.Write(Encoding.ASCII.GetBytes("data"));


            wr.Write((uint)liTailleData);
            //wr.Write(stream.GetBuffer());
            wr.Write(stream.GetBuffer());

            f.Close();
            
            return NomFichier;
        }
    }
}
