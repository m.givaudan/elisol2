﻿using System.Collections.Generic;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Elisol.Entity_Data_Models.ElisolModels
{
    [Table("t_liste_reduite_nematode", Schema = "public")]
    public class t_liste_reduite_nematode
    {
        [Key]
        public int id { get; set; }
        [ForeignKey("t_liste_reduite")]
        public Nullable<int> ilistereduite { get; set; }
        [ForeignKey("t_nematodes")]
        public Nullable<int> inematodes { get; set; }

        public virtual t_liste_reduite t_liste_reduite { get; set; }
        public virtual t_nematodes t_nematodes { get; set; }
    }
}
