﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Elisol.Entity_Data_Models.ElisolModels
{
    [Table("t_dictee", Schema = "public")]
    public class t_dictee
    {
        [Key]
        public int id { get; set; }
        public System.DateTime dheure { get; set; }
        public string cnom { get; set; }
        [ForeignKey("t_echantillons")]
        public Nullable<int> iechantillons { get; set; }
        [ForeignKey("t_nematodes")]
        public Nullable<int> inematodes { get; set; }

        public virtual t_nematodes t_nematodes { get; set; }
        public virtual t_echantillons t_echantillons { get; set; }
    }
}
