﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Elisol.Entity_Data_Models.ElisolModels
{
    [Table("t_dossiers", Schema = "public")]
    public class t_dossiers
    {
        [Key]
        public int id { get; set; }
        [ForeignKey("t_clients")]
        public Nullable<int> client_id { get; set; }
        public string cnom { get; set; }

        public virtual t_clients t_clients { get; set; }
        public virtual ICollection<t_echantillons> t_echantillons { get; set; }
    }
}
