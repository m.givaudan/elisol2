﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Elisol2.Entity_Data_Models.Elisol___PostgreSQL_NPGSQL
{
    [Table("tDictee", Schema = "public")]
    public class tDictee
    {
        [Key]
        public int iID_Dictee { get; set; }
        [ForeignKey("tEchantillons")]
        public Nullable<int> iEchantillon { get; set; }
        public Nullable<int> iNematode { get; set; }
        //public Nullable<System.DateTime> dHeure { get; set; }
        public string dHeure { get; set; }
        public Nullable<int> iUtilisateur { get; set; }
        public string cFichier_Son { get; set; }


        public virtual tEchantillons tEchantillons { get; set; }

        public virtual ICollection<tNematodes> tNematodes { get; set; }
        public virtual ICollection<tUtilisateurs> tUtilisateurs { get; set; }
    }
}
