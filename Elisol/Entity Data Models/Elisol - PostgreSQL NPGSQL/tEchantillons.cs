﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Elisol2.Entity_Data_Models.Elisol___PostgreSQL_NPGSQL
{
    [Table("tEchantillons", Schema = "public")]
    public class tEchantillons
    {
        [Key]
        public int iID_Echantillons { get; set; }
        [ForeignKey("tDossiers")]
        public Nullable<int> iDossier { get; set; }
        public string cNom { get; set; }
        public Nullable<int> iListe { get; set; }     
       
        public virtual tDossiers tDossiers { get; set; }

        public virtual ICollection<tListeReduite> tListeReduite { get; set; }
        public virtual ICollection<tDictee> tDictee { get; set; }
    }
}
