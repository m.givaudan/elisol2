﻿using System.Data.Entity;

namespace Elisol2.Entity_Data_Models.Elisol___PostgreSQL_NPGSQL
{
    class db_entities : DbContext
    {
        public db_entities() : base(nameOrConnectionString: "BDD") { }

        public DbSet<tClients> tClients { get; set; }
        public DbSet<tDictee> tDictee { get; set; }
        public DbSet<tDossiers> tDossiers { get; set; }
        public DbSet<tEchantillons> tEchantillons { get; set; }
        public DbSet<tListeReduite> tListeReduite { get; set; }
        public DbSet<tListeReduiteNema> tListeReduiteNema { get; set; }
        public DbSet<tNematodes> tNematodes { get; set; }
        public DbSet<tUtilisateurs> tUtilisateurs { get; set; }
    }
}
