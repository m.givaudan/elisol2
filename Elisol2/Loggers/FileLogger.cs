﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;

namespace Elisol2
{
    class FileLogger
    {
        protected readonly object Verrou = new object();
        private string fullName;
        public string FullName
        {
            get
            {
                return fullName;
            }

            set
            {
                fullName = value;
            }
        }

        public FileLogger(string FullName = null)
        {
            if (FullName == null)
            {
                this.FullName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\" + System.Diagnostics.Process.GetCurrentProcess().ProcessName + ".log";
            }
            else
            {
                this.FullName = fullName;
            }              
        }
        public void Log(string message)
        {
            DateTime date = DateTime.Now;
            lock (Verrou)
            {
                using (StreamWriter w = new StreamWriter(fullName, true))
                {
                    w.WriteLine( "\r\n\r\n" + date + "\r\n" +message);
                }
            }
        }
    }
}
