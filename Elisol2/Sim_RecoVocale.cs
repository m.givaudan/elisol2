﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Elisol2.Entity_Data_Models.Elisol___MySQL;
using System.IO;
using CsvHelper;
using CsvHelper.Configuration;

namespace Elisol2
{
    class Sim_RecoVocale
    {
        public static CsvConfiguration CsvConf = new CsvConfiguration { Delimiter = ";" };

        public Sim_RecoVocale()
        {
            // Constructeur
        }
        
        // crée une dictée
        public void GenerateDicteeRow(t_echantillons e, t_nematodes n)
        {
            using (db_entities c = new db_entities())
            {
                c.t_dictee.Add(new t_dictee() // créé une dictée
                {
                    cnom = e.cnom + "." + DateTime.Now.ToString("dd-MM-yy_HH.mm.ss") + ".wav",
                    inematodes = n.id,
                    iechantillons = e.id,
                    dheure = DateTime.Now
                });
                c.SaveChanges();
            }
        }

        // remplis et crée le fichier csv
        public void GenerateDicteeCsv(string path, List<CSVDictee> list)
        {
            if(File.Exists(path) == true) // si le fichier existe ...
            {
                using (TextWriter tr = new StreamWriter(path, true, Encoding.GetEncoding(1252)))
                {
                    var csv = new CsvWriter(tr, CsvConf);
                    foreach (var item in list)
                    {
                        csv.WriteField(item.NomDictee);
                        csv.WriteField(item.NomNematode);
                        csv.NextRecord();
                    }
                }
            }
            else // sinon ...
            {
                using (TextWriter tr = new StreamWriter(path, true, Encoding.GetEncoding(1252)))
                {
                    var csv = new CsvWriter(tr, CsvConf);
                    csv.WriteRecords(list);
                }
            }
        }

        // convertis une liste de nématodes en un tableau de string pour le dictionnaire de reconnaissance vocale
        public string[] GenerateTabForDictionnary(List<t_nematodes> list)
        {
            var tab = new string[list.Count];
            for (int i = 0; i < tab.Length; i++)
            {
                tab[i] = list[i].cnom_court;
            }
            return tab;
        }
    }
}
