﻿namespace Elisol2
{
    partial class Modifier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bAnnuler = new System.Windows.Forms.Button();
            this.bValider = new System.Windows.Forms.Button();
            this.gbDossier = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbNom_Dossier = new System.Windows.Forms.TextBox();
            this.gbEchantillon = new System.Windows.Forms.GroupBox();
            this.cbListReduiteNema = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.bAddList = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.tbNom_Echantillon = new System.Windows.Forms.TextBox();
            this.gbClient = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbNum_Siren = new System.Windows.Forms.TextBox();
            this.tbNom_Ville = new System.Windows.Forms.TextBox();
            this.tbCode_Postal = new System.Windows.Forms.TextBox();
            this.tbNum_Tel = new System.Windows.Forms.TextBox();
            this.tbNom_Entreprise = new System.Windows.Forms.TextBox();
            this.tbAdresse = new System.Windows.Forms.TextBox();
            this.gbDossier.SuspendLayout();
            this.gbEchantillon.SuspendLayout();
            this.gbClient.SuspendLayout();
            this.SuspendLayout();
            // 
            // bAnnuler
            // 
            this.bAnnuler.Location = new System.Drawing.Point(29, 87);
            this.bAnnuler.Margin = new System.Windows.Forms.Padding(4);
            this.bAnnuler.Name = "bAnnuler";
            this.bAnnuler.Size = new System.Drawing.Size(136, 55);
            this.bAnnuler.TabIndex = 1;
            this.bAnnuler.Text = "Annuler";
            this.bAnnuler.UseVisualStyleBackColor = true;
            this.bAnnuler.Click += new System.EventHandler(this.bAnnuler_Click);
            // 
            // bValider
            // 
            this.bValider.Location = new System.Drawing.Point(173, 87);
            this.bValider.Margin = new System.Windows.Forms.Padding(4);
            this.bValider.Name = "bValider";
            this.bValider.Size = new System.Drawing.Size(136, 55);
            this.bValider.TabIndex = 2;
            this.bValider.Text = "Valider";
            this.bValider.UseVisualStyleBackColor = true;
            this.bValider.Click += new System.EventHandler(this.bValider_Click);
            // 
            // gbDossier
            // 
            this.gbDossier.Controls.Add(this.label1);
            this.gbDossier.Controls.Add(this.tbNom_Dossier);
            this.gbDossier.Location = new System.Drawing.Point(16, 15);
            this.gbDossier.Margin = new System.Windows.Forms.Padding(4);
            this.gbDossier.Name = "gbDossier";
            this.gbDossier.Padding = new System.Windows.Forms.Padding(4);
            this.gbDossier.Size = new System.Drawing.Size(297, 65);
            this.gbDossier.TabIndex = 4;
            this.gbDossier.TabStop = false;
            this.gbDossier.Text = "Dossier";
            this.gbDossier.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 23);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Nom : ";
            // 
            // tbNom_Dossier
            // 
            this.tbNom_Dossier.Location = new System.Drawing.Point(156, 23);
            this.tbNom_Dossier.Margin = new System.Windows.Forms.Padding(4);
            this.tbNom_Dossier.Name = "tbNom_Dossier";
            this.tbNom_Dossier.Size = new System.Drawing.Size(132, 22);
            this.tbNom_Dossier.TabIndex = 2;
            // 
            // gbEchantillon
            // 
            this.gbEchantillon.Controls.Add(this.cbListReduiteNema);
            this.gbEchantillon.Controls.Add(this.label9);
            this.gbEchantillon.Controls.Add(this.bAddList);
            this.gbEchantillon.Controls.Add(this.label8);
            this.gbEchantillon.Controls.Add(this.tbNom_Echantillon);
            this.gbEchantillon.Location = new System.Drawing.Point(321, 15);
            this.gbEchantillon.Margin = new System.Windows.Forms.Padding(4);
            this.gbEchantillon.Name = "gbEchantillon";
            this.gbEchantillon.Padding = new System.Windows.Forms.Padding(4);
            this.gbEchantillon.Size = new System.Drawing.Size(297, 164);
            this.gbEchantillon.TabIndex = 6;
            this.gbEchantillon.TabStop = false;
            this.gbEchantillon.Text = "Echantillon";
            this.gbEchantillon.Visible = false;
            // 
            // cbListReduiteNema
            // 
            this.cbListReduiteNema.DisplayMember = "cNom";
            this.cbListReduiteNema.FormattingEnabled = true;
            this.cbListReduiteNema.Location = new System.Drawing.Point(12, 73);
            this.cbListReduiteNema.Margin = new System.Windows.Forms.Padding(4);
            this.cbListReduiteNema.Name = "cbListReduiteNema";
            this.cbListReduiteNema.Size = new System.Drawing.Size(276, 24);
            this.cbListReduiteNema.TabIndex = 32;
            this.cbListReduiteNema.ValueMember = "cNom";
            this.cbListReduiteNema.SelectionChangeCommitted += new System.EventHandler(this.cbListReduiteNema_SelectionChangeCommitted);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 52);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(240, 17);
            this.label9.TabIndex = 31;
            this.label9.Text = "Selectionnez une liste de nématodes";
            // 
            // bAddList
            // 
            this.bAddList.Location = new System.Drawing.Point(79, 106);
            this.bAddList.Margin = new System.Windows.Forms.Padding(4);
            this.bAddList.Name = "bAddList";
            this.bAddList.Size = new System.Drawing.Size(143, 42);
            this.bAddList.TabIndex = 30;
            this.bAddList.Text = "Créer une liste";
            this.bAddList.UseVisualStyleBackColor = true;
            this.bAddList.Click += new System.EventHandler(this.bAddList_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 23);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 17);
            this.label8.TabIndex = 4;
            this.label8.Text = "Nom : ";
            // 
            // tbNom_Echantillon
            // 
            this.tbNom_Echantillon.Location = new System.Drawing.Point(156, 23);
            this.tbNom_Echantillon.Margin = new System.Windows.Forms.Padding(4);
            this.tbNom_Echantillon.Name = "tbNom_Echantillon";
            this.tbNom_Echantillon.Size = new System.Drawing.Size(132, 22);
            this.tbNom_Echantillon.TabIndex = 2;
            // 
            // gbClient
            // 
            this.gbClient.Controls.Add(this.label7);
            this.gbClient.Controls.Add(this.label6);
            this.gbClient.Controls.Add(this.label5);
            this.gbClient.Controls.Add(this.label4);
            this.gbClient.Controls.Add(this.label3);
            this.gbClient.Controls.Add(this.tbAdresse);
            this.gbClient.Controls.Add(this.label2);
            this.gbClient.Controls.Add(this.tbNum_Siren);
            this.gbClient.Controls.Add(this.tbNom_Ville);
            this.gbClient.Controls.Add(this.tbCode_Postal);
            this.gbClient.Controls.Add(this.tbNum_Tel);
            this.gbClient.Controls.Add(this.tbNom_Entreprise);
            this.gbClient.Location = new System.Drawing.Point(321, 202);
            this.gbClient.Margin = new System.Windows.Forms.Padding(4);
            this.gbClient.Name = "gbClient";
            this.gbClient.Padding = new System.Windows.Forms.Padding(4);
            this.gbClient.Size = new System.Drawing.Size(297, 223);
            this.gbClient.TabIndex = 7;
            this.gbClient.TabStop = false;
            this.gbClient.Text = "Client";
            this.gbClient.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 178);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 17);
            this.label7.TabIndex = 14;
            this.label7.Text = "N°Siren :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 144);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 17);
            this.label6.TabIndex = 13;
            this.label6.Text = "Ville : ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 108);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 17);
            this.label5.TabIndex = 12;
            this.label5.Text = "Code Postal : ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 78);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 17);
            this.label4.TabIndex = 11;
            this.label4.Text = "Tel : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 19);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 17);
            this.label3.TabIndex = 10;
            this.label3.Text = "Nom du client : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 48);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 17);
            this.label2.TabIndex = 9;
            this.label2.Text = "Adresse : ";
            // 
            // tbNum_Siren
            // 
            this.tbNum_Siren.Location = new System.Drawing.Point(157, 178);
            this.tbNum_Siren.Margin = new System.Windows.Forms.Padding(4);
            this.tbNum_Siren.Name = "tbNum_Siren";
            this.tbNum_Siren.Size = new System.Drawing.Size(132, 22);
            this.tbNum_Siren.TabIndex = 6;
            // 
            // tbNom_Ville
            // 
            this.tbNom_Ville.Location = new System.Drawing.Point(156, 144);
            this.tbNom_Ville.Margin = new System.Windows.Forms.Padding(4);
            this.tbNom_Ville.Name = "tbNom_Ville";
            this.tbNom_Ville.Size = new System.Drawing.Size(132, 22);
            this.tbNom_Ville.TabIndex = 5;
            // 
            // tbCode_Postal
            // 
            this.tbCode_Postal.Location = new System.Drawing.Point(156, 109);
            this.tbCode_Postal.Margin = new System.Windows.Forms.Padding(4);
            this.tbCode_Postal.Name = "tbCode_Postal";
            this.tbCode_Postal.Size = new System.Drawing.Size(132, 22);
            this.tbCode_Postal.TabIndex = 4;
            // 
            // tbNum_Tel
            // 
            this.tbNum_Tel.Location = new System.Drawing.Point(156, 79);
            this.tbNum_Tel.Margin = new System.Windows.Forms.Padding(4);
            this.tbNum_Tel.Name = "tbNum_Tel";
            this.tbNum_Tel.Size = new System.Drawing.Size(132, 22);
            this.tbNum_Tel.TabIndex = 3;
            // 
            // tbNom_Entreprise
            // 
            this.tbNom_Entreprise.Location = new System.Drawing.Point(156, 16);
            this.tbNom_Entreprise.Margin = new System.Windows.Forms.Padding(4);
            this.tbNom_Entreprise.Name = "tbNom_Entreprise";
            this.tbNom_Entreprise.Size = new System.Drawing.Size(132, 22);
            this.tbNom_Entreprise.TabIndex = 2;
            // 
            // tbAdresse
            // 
            this.tbAdresse.Location = new System.Drawing.Point(156, 49);
            this.tbAdresse.Margin = new System.Windows.Forms.Padding(4);
            this.tbAdresse.Name = "tbAdresse";
            this.tbAdresse.Size = new System.Drawing.Size(132, 22);
            this.tbAdresse.TabIndex = 1;
            // 
            // Modifier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(656, 425);
            this.Controls.Add(this.gbClient);
            this.Controls.Add(this.gbEchantillon);
            this.Controls.Add(this.gbDossier);
            this.Controls.Add(this.bValider);
            this.Controls.Add(this.bAnnuler);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Modifier";
            this.Text = "Modifier";
            this.gbDossier.ResumeLayout(false);
            this.gbDossier.PerformLayout();
            this.gbEchantillon.ResumeLayout(false);
            this.gbEchantillon.PerformLayout();
            this.gbClient.ResumeLayout(false);
            this.gbClient.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button bAnnuler;
        private System.Windows.Forms.Button bValider;
        private System.Windows.Forms.GroupBox gbDossier;
        private System.Windows.Forms.TextBox tbNom_Dossier;
        private System.Windows.Forms.GroupBox gbEchantillon;
        private System.Windows.Forms.TextBox tbNom_Echantillon;
        private System.Windows.Forms.GroupBox gbClient;
        private System.Windows.Forms.TextBox tbNum_Siren;
        private System.Windows.Forms.TextBox tbNom_Ville;
        private System.Windows.Forms.TextBox tbCode_Postal;
        private System.Windows.Forms.TextBox tbNum_Tel;
        private System.Windows.Forms.TextBox tbNom_Entreprise;
        private System.Windows.Forms.TextBox tbAdresse;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbListReduiteNema;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button bAddList;
    }
}