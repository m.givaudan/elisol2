﻿using System;
using System.Collections.Generic;
using Elisol2.Entity_Data_Models.Elisol___MySQL;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;

namespace Elisol2
{
    public partial class Modifier : Form
    {
        t_clients client;
        t_dossiers dossier;
        t_echantillons echantillon;
        t_logs_repos desLogs;

        string NomListNema;
        t_liste_reduite LR;

        t_clients PrevClient;
        t_dossiers PrevDossier;
        t_echantillons PrevEchantillon;

        string type;
        bool create = false;

        public Modifier(string s)
        {
            InitializeComponent();
            type = s;
            create = true;

            #region IHM d'un client
            gbDossier.Hide();
            gbEchantillon.Hide();

            gbClient.Show();
            gbClient.Location = new Point(15, 15);

            bValider.Location = new Point(gbClient.Location.X, tbNum_Siren.Bottom + 40);
            bAnnuler.Location = new Point(bValider.Location.X + 120, tbNum_Siren.Bottom + 40);

            Size = new Size(gbClient.Right + 30, bValider.Bottom + 50);
            #endregion   
        }

        public Modifier(string s, t_clients c)
        {
            InitializeComponent();
            type = s;
            client = c;
            create = true;

            #region IHM d'un dossier
            gbClient.Hide();
            gbEchantillon.Hide();

            gbDossier.Show();
            gbDossier.Location = new Point(15, 15);

            bValider.Location = new Point(gbDossier.Location.X, tbNom_Dossier.Bottom + 40);
            bAnnuler.Location = new Point(bValider.Location.X + 120, tbNom_Dossier.Bottom + 40);

            Size = new Size(gbDossier.Right + 30, bValider.Bottom + 50);
            #endregion
        }

        public Modifier(string s, t_dossiers d)
        {
            InitializeComponent();
            type = s;
            dossier = d;
            create = true;

            #region IHM d'un echantillon
            using (var context = new db_entities())
            {
                var ListeReduite = context.t_liste_reduite.ToList();
                cbListReduiteNema.DataSource = ListeReduite;
                cbListReduiteNema.SelectedIndex = -1;
                cbListReduiteNema.Text = "Selectionnez une liste réduite";
            }

            gbClient.Hide();
            gbDossier.Hide();

            gbEchantillon.Show();
            gbEchantillon.Location = new Point(15, 15);

            bValider.Location = new Point(gbEchantillon.Location.X, bAddList.Bottom + 40);
            bAnnuler.Location = new Point(bValider.Location.X + 120, bAddList.Bottom + 40);

            Size = new Size(gbEchantillon.Right + 30, bValider.Bottom + 50);
            #endregion
        }

        public Modifier(t_clients c)
        {
            InitializeComponent();
            gbDossier.Hide();
            gbEchantillon.Hide();
            type = "t_clients";
            gbClient.Show();
            gbClient.Location = new Point(15, 15);
            client = c;

            #region Mise en place de la WinForm

            Text = c.cnom_entreprise;

            //tbID_Client.Text = c.iID_Clients.ToString();
            tbAdresse.Text = c.cadresse;
            tbNom_Entreprise.Text = c.cnom_entreprise;
            tbNum_Tel.Text = c.cnum_tel;
            tbNom_Ville.Text = c.cville;
            tbCode_Postal.Text = c.ccode_postal;
            tbNum_Siren.Text = c.cnum_siren;

            bValider.Location = new Point(gbClient.Location.X, tbNum_Siren.Bottom + 40);
            bAnnuler.Location = new Point(bValider.Location.X + 120, tbNum_Siren.Bottom + 40);

            Size = new Size(gbClient.Right + 30, bValider.Bottom + 50);
            #endregion
        }

        public Modifier(t_dossiers d)
        {
            InitializeComponent();
            gbClient.Hide();
            gbEchantillon.Hide();
            type = "t_dossiers";
            gbDossier.Show();
            gbDossier.Location = new Point(15, 15);
            dossier = d;

            #region Mise en place de la WinForm

            Text = d.cnom;

            tbNom_Dossier.Text = dossier.cnom;

            bValider.Location = new Point(gbDossier.Location.X, tbNom_Dossier.Bottom + 40);
            bAnnuler.Location = new Point(bValider.Location.X + 120, tbNom_Dossier.Bottom + 40);

            Size = new Size(gbDossier.Right + 30, bValider.Bottom + 50);

            #endregion
        }

        public Modifier(t_echantillons e)
        {
            InitializeComponent();
            gbClient.Hide();
            gbDossier.Hide();
            type = "t_echantillons";
            gbEchantillon.Show();
            gbEchantillon.Location = new Point(15, 15);
            echantillon = e;

            #region Mise en place de la WinForm

            Text = e.cnom;

            tbNom_Echantillon.Text = echantillon.cnom;

            bValider.Location = new Point(gbEchantillon.Location.X, tbNom_Echantillon.Bottom + 40);
            bAnnuler.Location = new Point(bValider.Location.X + 120, tbNom_Echantillon.Bottom + 40);

            Size = new Size(gbEchantillon.Right + 30, bValider.Bottom + 50);
            #endregion
        }

        private void cbListReduiteNema_SelectionChangeCommitted(object sender, EventArgs e)
        {
            NomListNema = cbListReduiteNema.SelectedValue.ToString();
            if (NomListNema != "Selectionnez une liste réduite")
            {
                using (var c = new db_entities())
                {
                    LR = c.t_liste_reduite.FirstOrDefault(lr => lr.cnom == cbListReduiteNema.SelectedValue.ToString());
                }
                bValider.Enabled = true;
            }
        }

        private void bAnnuler_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void bValider_Click(object sender, EventArgs e)
        {
            // faire l'update
            using (var c = new db_entities())
            {
                string textinfo = "Etes-vous sûr de vouloir valider ces informations ?";
                string info = "Voulez-vous vraiment créer ?";

                if (type == "t_clients")
                {
                    if (create == true)
                    {
                        #region Création d'un client
                        c.t_clients.Add(new t_clients()
                        {
                            cadresse = tbAdresse.Text,
                            cnom_entreprise = tbNom_Entreprise.Text,
                            cnum_tel = tbNum_Tel.Text,
                            cville = tbNom_Ville.Text,
                            ccode_postal = tbCode_Postal.Text,
                            cnum_siren = tbNum_Siren.Text
                        });
                        textinfo = info + tbNom_Entreprise.Text;
                        #endregion
                    }
                    else
                    {
                        #region Modification d'un client
                        string PrevAdd = client.cadresse;
                        client.cadresse = tbAdresse.Text;
                        string PrevNomEntreprise = client.cnom_entreprise;
                        client.cnom_entreprise = tbNom_Entreprise.Text;
                        string PrevNum_Tel = client.cnum_tel;
                        client.cnum_tel = tbNum_Tel.Text;
                        string PrevNom_Ville = client.cville;
                        client.cville = tbNom_Ville.Text;
                        string PrevCode_Postal = client.ccode_postal;
                        client.ccode_postal = tbCode_Postal.Text;
                        string PrevNum_Siren = client.cnum_siren;
                        client.cnum_siren = tbNum_Siren.Text;

                        t_clients PrevClient = new t_clients()
                        {
                            id = client.id,
                            cadresse = PrevAdd,
                            cnom_entreprise = PrevNomEntreprise,
                            cnum_tel = PrevNum_Tel,
                            cville = PrevNom_Ville,
                            ccode_postal = PrevCode_Postal,
                            cnum_siren = PrevNum_Siren
                        };

                        c.t_clients.Attach(client);
                        c.Entry(client).State = EntityState.Modified;

                        if (client.cadresse != PrevAdd)
                        {
                            textinfo = textinfo + "\n" + PrevAdd + " ==> " + client.cadresse;
                        }
                        if (client.cnom_entreprise != PrevNomEntreprise)
                        {
                            textinfo = textinfo + "\n" + PrevNomEntreprise + " ==> " + client.cnom_entreprise;
                        }
                        if (client.cnum_tel != PrevNum_Tel)
                        {
                            textinfo = textinfo + "\n" + PrevNum_Tel + " ==> " + client.cnum_tel;
                        }
                        if (client.cville.ToString() != PrevNom_Ville.ToString())
                        {
                            textinfo = textinfo + "\n" + PrevNom_Ville.ToString() + " ==> " + client.cville.ToString();
                        }
                        if (client.ccode_postal.ToString() != PrevCode_Postal.ToString())
                        {
                            textinfo = textinfo + "\n" + PrevCode_Postal.ToString() + " ==> " + client.ccode_postal.ToString();
                        }
                        if (client.cnum_siren.ToString() != PrevNum_Siren.ToString())
                        {
                            textinfo = textinfo + "\n" + PrevNum_Siren.ToString() + " ==> " + client.cnum_siren.ToString();
                        }
                        desLogs.Ajouter(new t_logs { table_origine = type, id_enreg = 1, id_type_log = 0, date_op = DateTime.Now }, "Modifier");
                        #endregion
                    }
                }
                if (type == "t_dossiers")
                {
                    if (create == true)
                    {
                        #region Création d'un dossier
                        c.t_dossiers.Add(new t_dossiers()
                        {
                            cnom = tbNom_Dossier.Text,
                            client_id = client.id
                        });
                        textinfo = info + tbNom_Dossier.Text;
                        #endregion
                    }
                    else
                    {
                        #region Modification d'un dossier
                        string PrevNom = dossier.cnom;
                        dossier.cnom = tbNom_Dossier.Text;

                        t_dossiers PrevDossier = new t_dossiers()
                        {
                            id = dossier.id,
                            cnom = PrevNom,
                            client_id = dossier.client_id
                        };

                        c.t_dossiers.Attach(dossier);
                        c.Entry(dossier).State = EntityState.Modified;

                        if (dossier.cnom != PrevNom)
                        {
                            textinfo = textinfo + "\n" + PrevNom + " ==> " + dossier.cnom;
                        }
                        #endregion
                    }
                }
                if (type == "t_echantillons")
                {
                    if (create == true)
                    {
                        #region Création d'un echantillon

                        c.t_echantillons.Add(new t_echantillons()
                        {
                            cnom = tbNom_Echantillon.Text,
                            tdossier_id = dossier.id,
                            tlistereduite_id = LR.id
                        });
                        textinfo = info + tbNom_Echantillon.Text;
                        #endregion
                    }
                    else
                    {
                        #region Modification d'un echantillon
                        string PrevNomEchantillon = echantillon.cnom;
                        echantillon.cnom = tbNom_Echantillon.Text;

                        t_echantillons PrevEchantillon = new t_echantillons()
                        {
                            id = echantillon.id,
                            cnom = PrevNomEchantillon,
                            tdossier_id = echantillon.tdossier_id,
                            tlistereduite_id = echantillon.tlistereduite_id
                        };

                        c.t_echantillons.Attach(echantillon);
                        c.Entry(echantillon).State = EntityState.Modified;

                        if (echantillon.cnom != PrevNomEchantillon)
                        {
                            textinfo = textinfo + "\n" + PrevNomEchantillon + " ==> " + echantillon.cnom;
                        }
                        #endregion
                    }
                }

                if (MessageBox.Show(this, textinfo, "!! Warning !!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes && textinfo != "Etes-vous sûr de vouloir valider ces informations ?")
                {
                    c.SaveChanges();
                    Close();
                }
                client = PrevClient;
                dossier = PrevDossier;
                echantillon = PrevEchantillon;
            }
        }

        private void bAddList_Click(object sender, EventArgs e)
        {
            Ajouter_liste_nematode newListNema = new Ajouter_liste_nematode();
            newListNema.Show();
        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void tbNum_Tel_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (tbNum_Tel.TextLength > 15) { }
            }
            catch (Exception error)
            {
                FileLogger logs = new FileLogger();
                logs.Log(error.ToString());
                MessageBox.Show("numéro trop long, merci de saisir 15 chiffres maxi");
            }           
        }

        private void tbCode_Postal_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (tbCode_Postal.TextLength > 6) {}
            }
            catch (Exception error)
            {
                FileLogger logs = new FileLogger();
                logs.Log(error.ToString());
                MessageBox.Show("code postal trop long, merci de saisir 6 chiffres maxi");
            }            
        }

        private void tbNom_Ville_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (tbNom_Ville.TextLength > 100) { }
            }
            catch (Exception error)
            {
                FileLogger logs = new FileLogger();
                logs.Log(error.ToString());
                MessageBox.Show("nom de ville trop long, merci de saisir avec 100 caractères maxi");
            }           
        }

        private void tbNum_Siren_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (tbNum_Siren.TextLength > 255){}
            }
            catch (Exception error)
            {
                FileLogger logs = new FileLogger();
                logs.Log(error.ToString());
                MessageBox.Show("Siren trop long, merci de saisir avec 255 caractères maxi");
            }
            
        }

        private void tbNom_Entreprise_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (tbNom_Entreprise.TextLength > 255) {}
            }
            catch (Exception error)
            {
                FileLogger logs = new FileLogger();
                logs.Log(error.ToString());
                MessageBox.Show("Nom de l'entreprise trop long, merci de saisir avec 255 caractères maxi");
            }
           
        }
    }
}
