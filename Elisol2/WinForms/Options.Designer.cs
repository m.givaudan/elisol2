﻿namespace Elisol2
{
    partial class Options
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bNouveau = new System.Windows.Forms.Button();
            this.bModifier = new System.Windows.Forms.Button();
            this.bSupprimer = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // bNouveau
            // 
            this.bNouveau.Location = new System.Drawing.Point(12, 12);
            this.bNouveau.Name = "bNouveau";
            this.bNouveau.Size = new System.Drawing.Size(116, 47);
            this.bNouveau.TabIndex = 0;
            this.bNouveau.Text = "Nouveau";
            this.bNouveau.UseVisualStyleBackColor = true;
            this.bNouveau.Click += new System.EventHandler(this.bNouveau_Click);
            // 
            // bModifier
            // 
            this.bModifier.Location = new System.Drawing.Point(12, 65);
            this.bModifier.Name = "bModifier";
            this.bModifier.Size = new System.Drawing.Size(116, 47);
            this.bModifier.TabIndex = 1;
            this.bModifier.Text = "Modifier";
            this.bModifier.UseVisualStyleBackColor = true;
            this.bModifier.Click += new System.EventHandler(this.bModifier_Click);
            // 
            // bSupprimer
            // 
            this.bSupprimer.Location = new System.Drawing.Point(12, 118);
            this.bSupprimer.Name = "bSupprimer";
            this.bSupprimer.Size = new System.Drawing.Size(116, 45);
            this.bSupprimer.TabIndex = 2;
            this.bSupprimer.Text = "Supprimer";
            this.bSupprimer.UseVisualStyleBackColor = true;
            this.bSupprimer.Click += new System.EventHandler(this.bSupprimer_Click);
            // 
            // Options
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(140, 175);
            this.Controls.Add(this.bSupprimer);
            this.Controls.Add(this.bModifier);
            this.Controls.Add(this.bNouveau);
            this.Name = "Options";
            this.Text = "Options";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bNouveau;
        private System.Windows.Forms.Button bModifier;
        private System.Windows.Forms.Button bSupprimer;
    }
}