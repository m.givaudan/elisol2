﻿namespace Elisol2
{
    partial class Ajouter_liste_nematode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.BAnnuler = new System.Windows.Forms.Button();
            this.BAddTransfert = new System.Windows.Forms.Button();
            this.BCancelTransfert = new System.Windows.Forms.Button();
            this.BCancelTransfertAll = new System.Windows.Forms.Button();
            this.BAddTransfertAll = new System.Windows.Forms.Button();
            this.BValider = new System.Windows.Forms.Button();
            this.DGVListNemaAdd = new System.Windows.Forms.DataGridView();
            this.NomNemaAdd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DGVListNema = new System.Windows.Forms.DataGridView();
            this.NomNema = new System.Windows.Forms.DataGridViewTextBoxColumn();
          //  this.elisolDataSet = new Elisol2.elisolDataSet();
            this.tNematodesBindingSource = new System.Windows.Forms.BindingSource(this.components);
         //   this.tNematodesTableAdapter = new Elisol2.elisolDataSetTableAdapters.tNematodesTableAdapter();
          //  this.tClientsTableAdapter1 = new Elisol2.elisolDataSetTableAdapters.tClientsTableAdapter();
            this.label1 = new System.Windows.Forms.Label();
            this.tbRecherche = new System.Windows.Forms.TextBox();
            this.tbNomListe = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DGVListNemaAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGVListNema)).BeginInit();
        //    ((System.ComponentModel.ISupportInitialize)(this.elisolDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tNematodesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // BAnnuler
            // 
            this.BAnnuler.Location = new System.Drawing.Point(13, 338);
            this.BAnnuler.Name = "BAnnuler";
            this.BAnnuler.Size = new System.Drawing.Size(240, 47);
            this.BAnnuler.TabIndex = 31;
            this.BAnnuler.Text = "Annuler";
            this.BAnnuler.UseVisualStyleBackColor = true;
            this.BAnnuler.Click += new System.EventHandler(this.BAnnuler_Click);
            // 
            // BAddTransfert
            // 
            this.BAddTransfert.Location = new System.Drawing.Point(259, 166);
            this.BAddTransfert.Name = "BAddTransfert";
            this.BAddTransfert.Size = new System.Drawing.Size(37, 23);
            this.BAddTransfert.TabIndex = 30;
            this.BAddTransfert.Text = ">";
            this.BAddTransfert.UseVisualStyleBackColor = true;
            this.BAddTransfert.Click += new System.EventHandler(this.BAddTransfert_Click);
            // 
            // BCancelTransfert
            // 
            this.BCancelTransfert.Location = new System.Drawing.Point(259, 195);
            this.BCancelTransfert.Name = "BCancelTransfert";
            this.BCancelTransfert.Size = new System.Drawing.Size(37, 23);
            this.BCancelTransfert.TabIndex = 29;
            this.BCancelTransfert.Text = "<";
            this.BCancelTransfert.UseVisualStyleBackColor = true;
            this.BCancelTransfert.Click += new System.EventHandler(this.BCancelTransfert_Click);
            // 
            // BCancelTransfertAll
            // 
            this.BCancelTransfertAll.Location = new System.Drawing.Point(259, 310);
            this.BCancelTransfertAll.Name = "BCancelTransfertAll";
            this.BCancelTransfertAll.Size = new System.Drawing.Size(37, 23);
            this.BCancelTransfertAll.TabIndex = 28;
            this.BCancelTransfertAll.Text = "<<";
            this.BCancelTransfertAll.UseVisualStyleBackColor = true;
            this.BCancelTransfertAll.Click += new System.EventHandler(this.BCancelTransfertAll_Click);
            // 
            // BAddTransfertAll
            // 
            this.BAddTransfertAll.Location = new System.Drawing.Point(259, 59);
            this.BAddTransfertAll.Name = "BAddTransfertAll";
            this.BAddTransfertAll.Size = new System.Drawing.Size(37, 23);
            this.BAddTransfertAll.TabIndex = 27;
            this.BAddTransfertAll.Text = ">>";
            this.BAddTransfertAll.UseVisualStyleBackColor = true;
            this.BAddTransfertAll.Click += new System.EventHandler(this.BAddTransfertAll_Click);
            // 
            // BValider
            // 
            this.BValider.Location = new System.Drawing.Point(302, 339);
            this.BValider.Name = "BValider";
            this.BValider.Size = new System.Drawing.Size(240, 47);
            this.BValider.TabIndex = 26;
            this.BValider.Text = "Valider";
            this.BValider.UseVisualStyleBackColor = true;
            this.BValider.Click += new System.EventHandler(this.BValider_Click);
            // 
            // DGVListNemaAdd
            // 
            this.DGVListNemaAdd.AllowUserToAddRows = false;
            this.DGVListNemaAdd.AllowUserToDeleteRows = false;
            this.DGVListNemaAdd.AllowUserToResizeColumns = false;
            this.DGVListNemaAdd.AllowUserToResizeRows = false;
            this.DGVListNemaAdd.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVListNemaAdd.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NomNemaAdd});
            this.DGVListNemaAdd.EnableHeadersVisualStyles = false;
            this.DGVListNemaAdd.Location = new System.Drawing.Point(302, 59);
            this.DGVListNemaAdd.MultiSelect = false;
            this.DGVListNemaAdd.Name = "DGVListNemaAdd";
            this.DGVListNemaAdd.ReadOnly = true;
            this.DGVListNemaAdd.RowHeadersVisible = false;
            this.DGVListNemaAdd.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGVListNemaAdd.Size = new System.Drawing.Size(240, 274);
            this.DGVListNemaAdd.TabIndex = 25;
            this.DGVListNemaAdd.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVListNemaAdd_CellClick);
            // 
            // NomNemaAdd
            // 
            this.NomNemaAdd.HeaderText = "NomNema";
            this.NomNemaAdd.Name = "NomNemaAdd";
            this.NomNemaAdd.ReadOnly = true;
            this.NomNemaAdd.Width = 220;
            // 
            // DGVListNema
            // 
            this.DGVListNema.AllowUserToAddRows = false;
            this.DGVListNema.AllowUserToDeleteRows = false;
            this.DGVListNema.AllowUserToResizeColumns = false;
            this.DGVListNema.AllowUserToResizeRows = false;
            this.DGVListNema.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVListNema.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NomNema});
            this.DGVListNema.EnableHeadersVisualStyles = false;
            this.DGVListNema.Location = new System.Drawing.Point(13, 59);
            this.DGVListNema.MultiSelect = false;
            this.DGVListNema.Name = "DGVListNema";
            this.DGVListNema.ReadOnly = true;
            this.DGVListNema.RowHeadersVisible = false;
            this.DGVListNema.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGVListNema.Size = new System.Drawing.Size(240, 274);
            this.DGVListNema.TabIndex = 24;
            this.DGVListNema.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVListNema_CellClick);
            // 
            // NomNema
            // 
            this.NomNema.DataPropertyName = "cNom_Complet";
            this.NomNema.HeaderText = "NomNema";
            this.NomNema.Name = "NomNema";
            this.NomNema.ReadOnly = true;
            this.NomNema.Width = 220;
            // 
            // elisolDataSet
            // 
          //  this.elisolDataSet.DataSetName = "elisolDataSet";
         //   this.elisolDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tNematodesBindingSource
            // 
            this.tNematodesBindingSource.DataMember = "tNematodes";
          //  this.tNematodesBindingSource.DataSource = this.elisolDataSet;
            // 
            // tNematodesTableAdapter
            // 
           // this.tNematodesTableAdapter.ClearBeforeFill = true;
            // 
            // tClientsTableAdapter1
            // 
           // this.tClientsTableAdapter1.ClearBeforeFill = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 32;
            this.label1.Text = "Rechercher :";
            // 
            // tbRecherche
            // 
            this.tbRecherche.Location = new System.Drawing.Point(87, 22);
            this.tbRecherche.Name = "tbRecherche";
            this.tbRecherche.Size = new System.Drawing.Size(166, 20);
            this.tbRecherche.TabIndex = 33;
            this.tbRecherche.TextChanged += new System.EventHandler(this.tbRecherche_TextChanged);
            // 
            // tbNomListe
            // 
            this.tbNomListe.Location = new System.Drawing.Point(376, 22);
            this.tbNomListe.Name = "tbNomListe";
            this.tbNomListe.Size = new System.Drawing.Size(166, 20);
            this.tbNomListe.TabIndex = 35;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(301, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 34;
            this.label2.Text = "Nom liste :";
            // 
            // Ajouter_liste_nematode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(558, 399);
            this.Controls.Add(this.tbNomListe);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbRecherche);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BAnnuler);
            this.Controls.Add(this.BAddTransfert);
            this.Controls.Add(this.BCancelTransfert);
            this.Controls.Add(this.BCancelTransfertAll);
            this.Controls.Add(this.BAddTransfertAll);
            this.Controls.Add(this.BValider);
            this.Controls.Add(this.DGVListNemaAdd);
            this.Controls.Add(this.DGVListNema);
            this.Name = "Ajouter_liste_nematode";
            this.Text = "Ajouter_liste_nematode";
            ((System.ComponentModel.ISupportInitialize)(this.DGVListNemaAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGVListNema)).EndInit();
          //  ((System.ComponentModel.ISupportInitialize)(this.elisolDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tNematodesBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BAnnuler;
        private System.Windows.Forms.Button BAddTransfert;
        private System.Windows.Forms.Button BCancelTransfert;
        private System.Windows.Forms.Button BCancelTransfertAll;
        private System.Windows.Forms.Button BAddTransfertAll;
        private System.Windows.Forms.Button BValider;
        private System.Windows.Forms.DataGridView DGVListNemaAdd;
        //private elisolDataSet elisolDataSet;
        private System.Windows.Forms.BindingSource tNematodesBindingSource;
        //private elisolDataSetTableAdapters.tNematodesTableAdapter tNematodesTableAdapter;
        //private elisolDataSetTableAdapters.tClientsTableAdapter tClientsTableAdapter1;
        private System.Windows.Forms.DataGridView DGVListNema;
        private System.Windows.Forms.DataGridViewTextBoxColumn NomNema;
        private System.Windows.Forms.DataGridViewTextBoxColumn NomNemaAdd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbRecherche;
        private System.Windows.Forms.TextBox tbNomListe;
        private System.Windows.Forms.Label label2;
    }
}