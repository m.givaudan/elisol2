﻿using Elisol2.Entity_Data_Models.Elisol___MySQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Elisol2
{
    static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                //Application.Run(new Ajouter_liste_nematode());
                Application.Run(new NemaNet());
            }
            catch(Exception e)
            {
                MessageBox.Show("Une erreur est survenue, veuillez contacter le support");
                FileLogger logs = new FileLogger();
                logs.Log(e.ToString());
            }
            /*db_entities context = new db_entities();
            // pour les logs
            t_logs unLog = new t_logs();    // un enregistrement
            t_logs_repos desLogs = new t_logs_repos(context);   // traitements sur la table
            List<t_logs> mesLogs = new List<t_logs>();  // liste des résultats

            mesLogs = desLogs.Selectionner_tout();
            // boucle d'affichage
            foreach (var lenouveau in mesLogs)
            {
                Console.WriteLine("Log : {0}", lenouveau);
            }

            // Ajouter
            //desLogs.Ajouter(new t_logs { table_origine = "table", id_enreg = 1, id_type_log = 1, date_op = DateTime.Now });
            //desLogs.Ajouter(new t_logs { table_origine = "clients", id_enreg = 1, id_type_log = 3, date_op = DateTime.Now });
            desLogs.Ajouter(new t_logs { table_origine = "letest", id_enreg = 1, id_type_log = 0, date_op = DateTime.Now }, "Modifier");

            Console.WriteLine("\r\n\r\n");
            //Afficher de nouveau
            foreach (var lenouveau in desLogs.Selectionner_tout())
            {
                Console.WriteLine("Log : " + lenouveau.id + " " + lenouveau.date_op.ToString() + " " + lenouveau.table_origine);
            }

            //Console.WriteLine("Touche pour continuer ...");
            //Console.ReadKey();*/

        }
    }
}
