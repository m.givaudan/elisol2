﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elisol2.Entity_Data_Models.Elisol___MySQL
{
    class t_logs_repos
    {
        t_logs lenreg; // dernier enregistrement utilisé
        db_entities monContext;

        public t_logs_repos(db_entities param)
        {
            this.monContext = param;
        }

        public List<t_logs> Selectionner_tout()
        {
                var logs = monContext.Logs.ToList();
                return logs;
        }
        public bool Ajouter(t_logs param, string param2)
        {
            switch (param2)
            {
                case "Ajouter":
                    param.id_type_log = 1;
                    break;

                case "Modifier":
                    param.id_type_log = 2;
                    break;

                case "Supprimer":
                    param.id_type_log = 3;
                    break;
            }
            monContext.Logs.Add(param);
            monContext.SaveChanges();
            return true;
            }

        /*
            public t_logs Selectionner_un(string param)
            {
                
            }
            public int Selectionner_un_id(string param)
            {
                return 0;
            }
            public List <t_logs> Selectionner_Selon()
            {
            }

            public bool Supprimer(int param)
            {
                return true;
            }
            public bool Supprimer_tout()
            {
                return true;
            }

            public bool Modifier(t_logs psrc, t_logs pdest)
            {
                return true;
            }

        }*/
    }
}
