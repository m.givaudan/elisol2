﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Elisol2.Entity_Data_Models.Elisol___MySQL
{
    [Table("t_clients", Schema = "public")]
    public class t_clients
    {
        [Key]
        public int id { get; set; }
        public string cnum_siren { get; set; }
        public string cnum_tel { get; set; }
        public string cnom_entreprise { get; set; }
        public string cadresse { get; set; }
        public string ccode_postal { get; set; }
        public string cville { get; set; }

        public virtual ICollection<t_dossiers> t_dossiers { get; set; }
    }
}
