﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Elisol2.Entity_Data_Models.Elisol___MySQL
{
    [Table("t_liste_reduite", Schema = "public")]
    public class t_liste_reduite
    {
        [Key]
        public int id { get; set; }
        public string cnom { get; set; }

        public virtual ICollection<t_echantillons> t_echantillons { get; set; }
        public virtual ICollection<t_liste_reduite_nematode> t_liste_reduite_nematode { get; set; }

    }
}
