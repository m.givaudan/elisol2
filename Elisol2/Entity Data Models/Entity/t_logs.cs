﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elisol2.Entity_Data_Models.Elisol___MySQL
{
    class t_logs
    {
        [Key]
        public int id { get; set; }
        public string table_origine { get; set; }
        public int id_enreg { get; set; }
        public int id_type_log { get; set; }
        public DateTime date_op { get; set; }
    }
}
