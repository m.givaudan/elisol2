﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elisol2.Entity_Data_Models.Elisol___MySQL
{
    class t_logs_type
    {
        [Key]
        public int id { get; set; }
        public string libelle_type { get; set; }
    }
}
