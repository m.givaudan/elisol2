﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Elisol2.Entity_Data_Models.Elisol___MySQL
{
    [Table("t_echantillons", Schema = "public")]
    public class t_echantillons
    {
        [Key]
        public int id { get; set; }
        public string cnom { get; set; }
        [ForeignKey("t_dossiers")]
        public Nullable<int> tdossier_id { get; set; }
        [ForeignKey("t_liste_reduite")]
        public Nullable<int> tlistereduite_id { get; set; }

        public virtual ICollection<t_dictee> t_dictee { get; set; }
        public virtual t_dossiers t_dossiers { get; set; }
        public virtual t_liste_reduite t_liste_reduite { get; set; }
    }
}
