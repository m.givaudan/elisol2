﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Elisol2.Entity_Data_Models.Elisol___PostgreSQL_NPGSQL
{
    [Table("tListeReduiteNema", Schema = "public")]
    public class tListeReduiteNema
    {
        [Key]
        [Column(Order = 1)]
        [ForeignKey("tNematodes")]
        public int iListe { get; set; }
        [Key]
        [Column(Order = 2)]
        [ForeignKey("tListeReduite")]
        public int iNema { get; set; }

        public virtual tNematodes tNematodes { get; set; }
        public virtual tListeReduite tListeReduite { get; set; }
    }
}
