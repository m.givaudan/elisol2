﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Elisol2.Entity_Data_Models.Elisol___PostgreSQL_NPGSQL
{
    [Table("tClients", Schema = "public")]
    public class tClients
    {
        [Key]
        public int iID_Clients { get; set; }
        public Nullable<int> iNum_Commande { get; set; }
        public Nullable<int> iNum_Siren { get; set; }
        public string cNum_Tel { get; set; }
        public string cNom_Entreprise { get; set; }
        public string cAdresse { get; set; }
        public Nullable<int> iCode_Postal { get; set; }

        public virtual ICollection<tDossiers> tDossiers { get; set; }
    }
}
