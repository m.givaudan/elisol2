﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Elisol2.Entity_Data_Models.Elisol___PostgreSQL_NPGSQL
{
    [Table("tNematodes", Schema = "public")]
    public class tNematodes
    {
        [Key]
        public int iID_Nematodes { get; set; }
        public string cNom_Complet { get; set; }
        public string cNom_Dictable { get; set; }
        public string cNom_Court { get; set; }
        public string cPhonetique { get; set; }
        [ForeignKey("tDictee")]
        public Nullable<int> iID_Dictee { get; set; }

        
        public virtual tDictee tDictee { get; set; }

        public virtual ICollection<tListeReduiteNema> tListeReduiteNema { get; set; }
    }
}
