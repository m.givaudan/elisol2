﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Elisol2.Entity_Data_Models.Elisol___PostgreSQL_NPGSQL
{
    [Table("tListeReduite", Schema = "public")]
    public class tListeReduite
    {
        [Key]
        public int iID_ListeReduite { get; set; }
        public string cNom { get; set; }
        [ForeignKey("tEchantillons")]
        public Nullable<int> iID_Echantillon { get; set; }
        
        public virtual tEchantillons tEchantillons { get; set; }

       // public virtual ICollection<tNematodes> tNematodes { get; set; }
        public virtual ICollection<tListeReduiteNema> tListeReduiteNema { get; set; }

    }
}
